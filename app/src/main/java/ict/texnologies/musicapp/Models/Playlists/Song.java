package ict.texnologies.musicapp.Models.Playlists;

import com.google.firebase.database.Exclude;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Song extends ArrayList<Song> implements Serializable {
    private String nameOfSong;
    private String nameOfSinger;
    private String songUrl;
    private String songID;
    private ArrayList<String> map;
    public String getSongID() {
        return songID;
    }

    public void setSongID(String songID) {
        this.songID = songID;
    }

    public Song (){
    }
    public Song(String nameOfSong, String nameOfSinger,String songUrl) {
        this.nameOfSong = nameOfSong;
        this.nameOfSinger = nameOfSinger;
        this.songUrl = songUrl;
    }
    public Song(String nameOfSong, String nameOfSinger,String songUrl,String songID) {
        this.nameOfSinger = nameOfSinger;
        this.nameOfSong = nameOfSong;
        this.songID=songID;
        this.songUrl = songUrl;
    }
public Song(ArrayList<String> map){
        this.nameOfSinger=map.get(0);
}
    public String getNameOfSong() {

        return nameOfSong;
    }
    public void setNameOfSong(String nameOfSong) {

        this.nameOfSong = nameOfSong;
    }
    public String getNameOfSinger() {

        return nameOfSinger;
    }
    public void setNameOfSinger(String nameOfSinger) {
        this.nameOfSinger = nameOfSinger;
    }
    public String getSongUrl() {
        return songUrl;
    }

    public void setSongUrl(String songUrl) {
        this.songUrl = songUrl;
    }

    @Override
    public String toString() {
        return "Song{" +
                "nameOfSong='" + nameOfSong + '\'' +
                ", nameOfSinger='" + nameOfSinger + '\'' +
                ", songUrl='" + songUrl + '\'' +
                '}';
    }
    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("nameOfSinger", nameOfSinger);
        result.put("nameOfSong", nameOfSong);
       // result.put("songID", songID);
        result.put("songUrl", songUrl);

        return result;
    }
}
