package ict.texnologies.musicapp.Models.TopArtists;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class TopArtistsList {

    @SerializedName("artist")
    @Expose
    private List<TopArtistsDTO> artist = null;
    @SerializedName("@attr")
    @Expose
    private Attr attr;

    public List<TopArtistsDTO> getArtist() { return artist; }

    public void setArtist(List<TopArtistsDTO> artist) { this.artist = artist; }

    public Attr getAttr() { return attr; }

    public void setAttr(Attr attr) { this.attr = attr; }

}
