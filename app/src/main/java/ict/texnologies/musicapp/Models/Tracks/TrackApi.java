package ict.texnologies.musicapp.Models.Tracks;


import retrofit2.Call;
import retrofit2.http.GET;

public interface TrackApi {

    @GET("?method=chart.gettoptracks&api_key=00599a9ea8312cefa948ac8818a87c9f&format=json")
    Call<TopTracks> getAllTracks();
}
