package ict.texnologies.musicapp.Models.ArtistSearch;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SearchTrackAPI {

    @GET("?method=track.search&api_key=b5c5619f84aef03b09a17eb6b7fb9670&format=json")
    Call<Search> getSearchTrack(@Query("track")String track);

}
