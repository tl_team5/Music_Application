package ict.texnologies.musicapp.Models.Playlists;

import java.io.Serializable;
import java.util.ArrayList;

public class Playlist implements Serializable {
    private String nameOfPlaylist;
    private ArrayList<Song> songs;

    public String getIsPublic() {
        return isPublic;
    }

    public void setIsPublic(String isPublic) {
        this.isPublic = isPublic;
    }

    private String isPublic;

    public Playlist() {
    }

 public Playlist(String nameOfPlaylist) {
        this.nameOfPlaylist = nameOfPlaylist;
    }

    public Playlist(String nameOfPlaylist, ArrayList<Song> songs) {
        this.nameOfPlaylist = nameOfPlaylist;
        this.songs = songs;
    }

    public String getNameOfPlaylist() {
        return nameOfPlaylist;
    }

    public void setNameOfPlaylist(String nameOfPlaylist) {
        this.nameOfPlaylist = nameOfPlaylist;
    }

    public ArrayList<Song> getSongs() {
        return songs;
    }

    public void setSongs(ArrayList<Song> songs) {
        this.songs = songs;
    }

}
