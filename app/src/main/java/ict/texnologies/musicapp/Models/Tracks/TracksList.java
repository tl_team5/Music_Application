package ict.texnologies.musicapp.Models.Tracks;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;



public class TracksList {

    @SerializedName("track")
    @Expose
    private List<TrackDTO> track = null;
    @SerializedName("@attr")
    @Expose
    private Attr attr;

    public List<TrackDTO> getTrack() {
        return track;
    }

    public void setTrack(List<TrackDTO> track) {
        this.track = track;
    }

    public Attr getAttr() {
        return attr;
    }

    public void setAttr(Attr attr) {
        this.attr = attr;
    }

}