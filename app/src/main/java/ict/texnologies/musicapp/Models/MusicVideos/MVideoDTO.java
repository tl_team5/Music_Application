package ict.texnologies.musicapp.Models.MusicVideos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MVideoDTO {
    @SerializedName("idArtist")
    @Expose
    private String idArtist;
    @SerializedName("idAlbum")
    @Expose
    private String idAlbum;
    @SerializedName("idTrack")
    @Expose
    private String idTrack;
    @SerializedName("strTrack")
    @Expose
    private String strTrack;
    @SerializedName("strTrackThumb")
    @Expose
    private String strTrackThumb;
    @SerializedName("strMusicVid")
    @Expose
    private String strMusicVid;
    @SerializedName("strDescriptionEN")
    @Expose
    private String strDescriptionEN;

    public String getIdArtist() {
        return idArtist;
    }

    public void setIdArtist(String idArtist) {
        this.idArtist = idArtist;
    }

    public String getIdAlbum() {
        return idAlbum;
    }

    public void setIdAlbum(String idAlbum) {
        this.idAlbum = idAlbum;
    }

    public String getIdTrack() {
        return idTrack;
    }

    public void setIdTrack(String idTrack) {
        this.idTrack = idTrack;
    }

    public String getStrTrack() {
        return strTrack;
    }

    public void setStrTrack(String strTrack) {
        this.strTrack = strTrack;
    }

    public String getStrTrackThumb() {
        return strTrackThumb;
    }

    public void setStrTrackThumb(String strTrackThumb) {
        this.strTrackThumb = strTrackThumb;
    }

    public String getStrMusicVid() {
        return strMusicVid;
    }

    public void setStrMusicVid(String strMusicVid) {
        this.strMusicVid = strMusicVid;
    }

    public String getStrDescriptionEN() {
        return strDescriptionEN;
    }

    public void setStrDescriptionEN(String strDescriptionEN) {
        this.strDescriptionEN = strDescriptionEN;
    }

}

