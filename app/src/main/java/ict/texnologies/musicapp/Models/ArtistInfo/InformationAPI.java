package ict.texnologies.musicapp.Models.ArtistInfo;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface InformationAPI {

    @GET("api/v1/json/2/search.php")
    Call<ArtistInfoList> getAllInfo(@Query("s")String artist);

}
