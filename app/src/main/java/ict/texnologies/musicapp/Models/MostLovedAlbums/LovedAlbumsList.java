package ict.texnologies.musicapp.Models.MostLovedAlbums;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LovedAlbumsList {
    @SerializedName("loved")
    @Expose
    private List<LovedAlbumsDTO> lovedAlbumsDTO = null;

    public List<LovedAlbumsDTO> getLovedAlbumsDTO() {
        return lovedAlbumsDTO;
    }

    public void setLovedAlbumsDTO(List<LovedAlbumsDTO> lovedAlbumsDTO) {
        this.lovedAlbumsDTO = lovedAlbumsDTO;
    }
}
