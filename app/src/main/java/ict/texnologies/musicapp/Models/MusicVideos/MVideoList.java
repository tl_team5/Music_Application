package ict.texnologies.musicapp.Models.MusicVideos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MVideoList {

    @SerializedName("mvids")
    @Expose
    private List<MVideoDTO> mvids = null;

    public List<MVideoDTO> getMvids() {
        return mvids;
    }

    public void setMvids(List<MVideoDTO> mvids) {
        this.mvids = mvids;
    }

}

