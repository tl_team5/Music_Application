package ict.texnologies.musicapp.Models.TopArtists;

import retrofit2.Call;
import retrofit2.http.GET;

public interface TopArtistsAPI {

    @GET("?method=chart.gettopartists&api_key=b5c5619f84aef03b09a17eb6b7fb9670&format=json")
    Call<VeryTopArtists> getAllTopArtists();
}
