package ict.texnologies.musicapp.Models.Tracks;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopTracks {

    @SerializedName("tracks")
    @Expose
    private TracksList tracks;

    public TracksList getTracks() {
        return tracks;
    }

    public void setTracks(TracksList tracks) {
        this.tracks = tracks;
    }

}
