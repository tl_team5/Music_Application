package ict.texnologies.musicapp.Models.ArtistDiscography;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AlbumList {
    @SerializedName("album")
    @Expose
    private List<AlbumDTO> album = null;

    public AlbumList(List<AlbumDTO> album) {
        this.album = album;
    }

    public List<AlbumDTO> getAlbum() {
        return album;
    }

    public void setAlbum(List<AlbumDTO> album) {
        this.album = album;
    }
}
