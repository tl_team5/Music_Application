package ict.texnologies.musicapp.Models.MusicVideos;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface VideoApi {
    @GET("api/v1/json/2/mvid.php")
    Call<MVideoList> getAllVideos(@Query("i")int artistid);
}
