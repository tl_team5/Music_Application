package ict.texnologies.musicapp.Models.AlbumSearch;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SearchAlbumAPI {
    @GET("?method=album.search&api_key=8cbcc904b8ddab66a883154daf9261ca&format=json")
    Call<Search> getSearchAlbum(@Query("album")String album);
}

