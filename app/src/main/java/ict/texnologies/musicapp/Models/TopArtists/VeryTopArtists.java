package ict.texnologies.musicapp.Models.TopArtists;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class    VeryTopArtists {
    @SerializedName("artists")
    @Expose
    private TopArtistsList artists;

    public TopArtistsList getTopArtists() {
        return artists;
    }

    public void setTopArtists(TopArtistsList TopArtists) { this.artists = artists; }


}
