package ict.texnologies.musicapp.Models.ArtistInfo;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ArtistInfoList {

    @SerializedName("artists")
    @Expose
    private List<InformationDTO> information = null;

    public List<InformationDTO> getInformation() {
        return information;
    }

    public void setInformation(List<InformationDTO> information) {
        this.information = information;
    }
}