package ict.texnologies.musicapp.Models.ArtistSearch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchTracksList {
    @SerializedName("track")
    @Expose
    private List<SearchTrackDTO> track = null;

    public List<SearchTrackDTO> getTracks() {
        return track;
    }

    public void setTrack(List<SearchTrackDTO> track) {
        this.track = track;
    }
}
