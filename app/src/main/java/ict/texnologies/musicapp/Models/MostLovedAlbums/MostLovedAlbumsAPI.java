package ict.texnologies.musicapp.Models.MostLovedAlbums;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface MostLovedAlbumsAPI {

    @GET("api/v1/json/523532/mostloved.php?format=album")
    Call<LovedAlbumsList> getAllInfo();



}