package ict.texnologies.musicapp.Models.TopArtists;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopArtists {

    @SerializedName("artists")
    @Expose
    private TopArtistsList artists;

    public TopArtistsList getArtists() {
        return artists;
    }

    public void setArtists(TopArtistsList artists) { this.artists = artists; }
}