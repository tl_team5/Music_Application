package ict.texnologies.musicapp.Models.AlbumSearch;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchAlbumList {
    @SerializedName("album")
    @Expose
    private List<SearchAlbumDTO> album = null;

    public List<SearchAlbumDTO> getAlbum() {
        return album;
    }

    public void setAlbum(List<SearchAlbumDTO> album) {
        this.album = album;
    }
}
