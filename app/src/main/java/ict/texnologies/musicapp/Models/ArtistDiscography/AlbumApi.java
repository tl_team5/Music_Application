package ict.texnologies.musicapp.Models.ArtistDiscography;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface AlbumApi {

    @GET("/api/v1/json/2/discography.php")
    Call <AlbumList> getAllAlbums(@Query("s")String artist);
}
