package ict.texnologies.musicapp.ViewModels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import ict.texnologies.musicapp.Models.Playlists.Playlist;
import ict.texnologies.musicapp.Models.Playlists.Song;

public class SharedViewModel extends ViewModel {
    private MutableLiveData<Song> songMutableLiveData =new MutableLiveData<>();
    private MutableLiveData<Playlist> playlistMutableLiveData= new MutableLiveData<>();

    public void setSongMutableLiveData(Song song){
        songMutableLiveData.setValue(song);
    }
    public MutableLiveData<Song> getSongMutableLiveData() {
        if (songMutableLiveData==null){
            songMutableLiveData= new MutableLiveData<>();
        }
        return songMutableLiveData;
    }

    public void setPlaylistMutableLiveData(Playlist playlist){playlistMutableLiveData.setValue(playlist); }
    public MutableLiveData<Playlist> getPlaylistMutableLiveData(){
        if (playlistMutableLiveData==null)
        {
            playlistMutableLiveData= new MutableLiveData<>();
        }
        return playlistMutableLiveData;
    }
}
