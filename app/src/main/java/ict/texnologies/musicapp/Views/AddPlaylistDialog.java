package ict.texnologies.musicapp.Views;


import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import ict.texnologies.musicapp.Adapters.AddPlaylistDialogAdapter;
import ict.texnologies.musicapp.Models.Playlists.Playlist;
import ict.texnologies.musicapp.Models.Playlists.Song;
import ict.texnologies.musicapp.R;

public class AddPlaylistDialog extends DialogFragment implements AddPlaylistDialogAdapter.OnPlaylistListener{
    private RecyclerView playlistRecyclerView;
    private EditText editTextPlaylistname;
    private Button btnCreatePlaylist;

    ArrayList<Playlist> playlistsdb = new ArrayList<>();
    FirebaseDatabase database = FirebaseDatabase.getInstance("https://music-app-2604f-default-rtdb.europe-west1.firebasedatabase.app");
    DatabaseReference myRef = database.getReference();
    String userID;
    Song song2add;
    public AddPlaylistDialog(Song song) {
        super(R.layout.dialog_addplaylist);
        song2add=song;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        userID = user.getUid();
        playlistRecyclerView = view.findViewById(R.id.playlist_select_rec);
        editTextPlaylistname = view.findViewById(R.id.playlist_name_text);
        btnCreatePlaylist = view.findViewById(R.id.button_createpl);
        btnCreatePlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myRef.child("users").child(userID).child("playlists").child(editTextPlaylistname.getText().toString()).child(song2add.getNameOfSong()).setValue(song2add.toMap());
                AddPlaylistDialog.this.dismiss();

            }
        });
        ValueEventListener valueEventListener = new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                playlistsdb.clear();
                for (DataSnapshot snapshot : dataSnapshot.child("users").child(userID).child("playlists").getChildren()) {
                    ArrayList<Song> songstemp = new ArrayList<>();
                    String playlistnm = snapshot.getKey();
                    songstemp.clear();
                    for (DataSnapshot song : snapshot.getChildren()) {
                        songstemp.add(new Song(song.child("nameOfSong").getValue().toString(), song.child("nameOfSinger").getValue().toString(), song.child("songUrl").getValue().toString()));
                    }
                    playlistsdb.add(new Playlist(playlistnm, songstemp));
                }
                AddPlaylistDialogAdapter adapter = new AddPlaylistDialogAdapter(playlistsdb, AddPlaylistDialog.this);
                playlistRecyclerView.setAdapter(adapter);
                playlistRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                System.out.println("Error reading playlist from db");
            }
        };
        myRef.addValueEventListener(valueEventListener);
    }

    @Override
    public void onPlaylistClick(int position) {
        myRef.child("users").child(userID).child("playlists").child(playlistsdb.get(position).getNameOfPlaylist()).child(song2add.getNameOfSong()).setValue(song2add.toMap());
this.dismiss();
    }
}
