package ict.texnologies.musicapp.Views;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import ict.texnologies.musicapp.Adapters.SongsRecyclerViewAdapter;
import ict.texnologies.musicapp.Models.Playlists.Playlist;
import ict.texnologies.musicapp.R;
import ict.texnologies.musicapp.ViewModels.SharedViewModel;

public class AddSongsFragment extends Fragment implements SongsRecyclerViewAdapter.OnSongListener {

    private RecyclerView songRecyclerView;
    private TextView playlistname;
    private SharedViewModel sharedViewModel;
    private Button button_play;
    Playlist playlist_rec;
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        }
    Observer<Playlist> addedPlaylistObserver= new Observer<Playlist>() {
        @Override
        public void onChanged(Playlist playlist) {
        playlist_rec=playlist;
        }
    };
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_songs, container, false);
    }

    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sharedViewModel = new ViewModelProvider(requireActivity()).get(SharedViewModel.class);
        sharedViewModel.getPlaylistMutableLiveData().observe(requireActivity(),addedPlaylistObserver);
        button_play=view.findViewById(R.id.button_play);
        playlistname = view.findViewById(R.id.textViewPlaylistName);
        songRecyclerView = view.findViewById(R.id.songRecyclerView);
//        assert playlist_rec != null;
        playlistname.append(playlist_rec.getNameOfPlaylist());
        SongsRecyclerViewAdapter adapter = new SongsRecyclerViewAdapter(playlist_rec.getSongs(),this);
        songRecyclerView.setAdapter(adapter);
        songRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        button_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment_player = new VideoPlayerFragment();

                getParentFragmentManager().beginTransaction()
                        .add(((ViewGroup)getView().getParent()).getId(),fragment_player)
                        .addToBackStack(null)
                        .commit();
            }
        });
    }

    @Override
    public void onSongClick(int position) {
        //TODO: P Play selected Song (using url) in VideoPlayerFragment
    }

}