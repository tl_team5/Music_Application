package ict.texnologies.musicapp.Views;

import static android.content.ContentValues.TAG;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ict.texnologies.musicapp.Adapters.ArtistSearchAdapter;
import ict.texnologies.musicapp.Clients.AudioDBClient;
import ict.texnologies.musicapp.Models.ArtistInfo.InformationAPI;
import ict.texnologies.musicapp.Models.ArtistInfo.InformationDTO;
import ict.texnologies.musicapp.Models.ArtistInfo.ArtistInfoList;
import ict.texnologies.musicapp.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArtistSearchFragment extends Fragment implements ArtistSearchAdapter.OnItemListener {


    Button buttonGetInfo;
    RecyclerView recyclerView;
    EditText searchText;
    LinearLayoutManager layoutManager;
    ArtistSearchAdapter adapter;
    List<InformationDTO> infoList = new ArrayList<>();
    private NavController navController;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState){
        return inflater.inflate(R.layout.fragment_search_artist,container,false);
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController = Navigation.findNavController(view);
        recyclerView = view.findViewById(R.id.recyclerViewArtists);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new ArtistSearchAdapter(infoList,this);
        recyclerView.setAdapter(adapter);

        searchText = view.findViewById(R.id.searchName);
        searchText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchText.setText(null);
            }
        });

        buttonGetInfo = view.findViewById(R.id.buttonGetInfo);
        buttonGetInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!searchText.getText().toString().isEmpty()){
                try {
                    fetchInformation(searchText.getText().toString() );
                } catch (IOException e) {
                    e.printStackTrace();
                }
                }
            }
        });

    }

        private void fetchInformation(String searchFilter) throws IOException {
        InformationAPI informationAPI = AudioDBClient.getRetrofitInstance().create(InformationAPI.class);
        Call<ArtistInfoList> allInfo = informationAPI.getAllInfo(searchFilter);
        allInfo.enqueue(new Callback<ArtistInfoList>() {
            @Override
            public void onResponse(Call<ArtistInfoList> call, Response<ArtistInfoList> response) {
                Log.e("TAG","onResponse:code :"+response.code());

                ArtistInfoList artistInfoList = response.body();
                List<InformationDTO> information = artistInfoList.getInformation();
                if(information !=null) {
                    for (InformationDTO informationDTO : information) {
                        System.out.println(informationDTO.getStrBiographyEN());
                        infoList.add(informationDTO);
                        adapter.notifyDataSetChanged();
                    }

                }

            }

            @Override
            public void onFailure(Call<ArtistInfoList> call, Throwable t) {
                Log.e("TAG","onResponse:failure :"+t.getMessage());
            }
        });
    }

    @Override
    public void onItemClick(int position) {
        Log.d(TAG,"onItemClick: clicked");
        Log.d(TAG, String.valueOf(position));
        String idArtist = infoList.get(position).getIdArtist();
        String nameArtist = infoList.get(position).getStrArtist();
        ArtistSearchFragmentDirections.ActionArtistSearchFragmentToArtistDetailsFragment action= ArtistSearchFragmentDirections.actionArtistSearchFragmentToArtistDetailsFragment(idArtist,nameArtist);
        navController.navigate(action);
    }
}