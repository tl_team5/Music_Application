package ict.texnologies.musicapp.Views;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;

import ict.texnologies.musicapp.Adapters.PlaylistsRecyclerViewAdapter;
import ict.texnologies.musicapp.Models.Playlists.Playlist;
import ict.texnologies.musicapp.Models.Playlists.Song;
import ict.texnologies.musicapp.R;
import ict.texnologies.musicapp.ViewModels.SharedViewModel;

public class AddPlaylistsFragment extends Fragment implements PlaylistsRecyclerViewAdapter.OnPlaylistListener {

    private RecyclerView playlistRecyclerView;
    private EditText editTextPlaylist;
    private Button btnAddPlaylist;
    private TextView txtWarnPlaylist;

    private SharedViewModel sharedViewModel;
    ArrayList<Playlist> playlistsdb = new ArrayList<>();
    ArrayList<Song> songs = new ArrayList<>();
    FirebaseDatabase database = FirebaseDatabase.getInstance("https://music-app-2604f-default-rtdb.europe-west1.firebasedatabase.app");
    DatabaseReference myRef = database.getReference();
    String userID;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        userID = user.getUid();

        }
Observer<Song> addedSongObserver= new Observer<Song>() {
    @Override
    public void onChanged(Song song) {
        songs.add(song);
    }
};
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_playlists, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        playlistRecyclerView = view.findViewById(R.id.playlistRecyclerView);
        editTextPlaylist = view.findViewById(R.id.editTextPlaylist);
        btnAddPlaylist = view.findViewById(R.id.btnAddPlaylist);
        txtWarnPlaylist = view.findViewById(R.id.txtWarnPlaylist);
        sharedViewModel = new ViewModelProvider(requireActivity()).get(SharedViewModel.class);
        sharedViewModel.getSongMutableLiveData().observe(getActivity(),addedSongObserver);
        ValueEventListener valueEventListener = new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                playlistsdb.clear();
                for (DataSnapshot snapshot : dataSnapshot.child("users").child(userID).child("playlists").getChildren()) {
                    ArrayList<Song> songstemp = new ArrayList<>();
                    String playlistnm= snapshot.getKey();
                    songstemp.clear();
                    for(DataSnapshot song : snapshot.getChildren())
                    {
                        songstemp.add(new Song(song.child("nameOfSong").getValue().toString(),song.child("nameOfSinger").getValue().toString(),song.child("songUrl").getValue().toString()));
                    }
                    playlistsdb.add(new Playlist(playlistnm,songstemp));
                }
                PlaylistsRecyclerViewAdapter adapter = new PlaylistsRecyclerViewAdapter(playlistsdb, AddPlaylistsFragment.this);
                playlistRecyclerView.setAdapter(adapter);
                playlistRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                System.out.println("Error reading playlist from db");
            }
        };
        myRef.addValueEventListener(valueEventListener);

        btnAddPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateData();
                showToast();
            }
        });

    }

    private void addPlaylist() {
        String plname = editTextPlaylist.getText().toString();
        Switch ispublic_switch = getView().findViewById(R.id.switch_public_playlist);
        if (ispublic_switch.isChecked()) {
            HashMap<String, Object> playlistmap = new HashMap<>();
            playlistmap.put("playlist", plname);
            playlistmap.put("isPublic", "true");
            playlistmap.put("userID", userID);
            myRef.child("public_playlists").push().child(plname).setValue(playlistmap);
        }
        for (Song song: songs)
        myRef.child("users").child(userID).child("playlists").child(plname).child(song.getNameOfSong()).setValue(song.toMap());
    }

    @Override
    public void onPlaylistClick(int position) {
        sharedViewModel.setPlaylistMutableLiveData(new Playlist(playlistsdb.get(position).getNameOfPlaylist(), playlistsdb.get(position).getSongs()));
        Fragment fragment_addsongs = new AddSongsFragment();
        FragmentTransaction fragmentTransaction = getParentFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.library_fragment, fragment_addsongs).addToBackStack(null);
        fragmentTransaction.commit();
    }

    public Playlist getPlaylistsdblaylist(int index) {
        return playlistsdb.get(index);
    }

    private boolean validateData() {
        if (editTextPlaylist.getText().toString().equals("")) {
            txtWarnPlaylist.setVisibility(View.VISIBLE);
            return false;
        }
        return true;
    }

    private void showToast() {
        if (validateData()) {

            boolean existPlaylist = false;
            String nameOfPlaylist = editTextPlaylist.getText().toString();
            for (Playlist p : playlistsdb) {
                if (p.getNameOfPlaylist().equals(nameOfPlaylist)) {
                    existPlaylist = true;
                }
            }
            if (existPlaylist) {
                txtWarnPlaylist.setVisibility(View.GONE);
                editTextPlaylist.getText().clear();
                Toast.makeText(getContext(), "Playlist already exists!!", Toast.LENGTH_SHORT).show();
            } else {

                txtWarnPlaylist.setVisibility(View.GONE);
                addPlaylist();
                editTextPlaylist.getText().clear();
                Toast.makeText(getContext(), "Playlist " + nameOfPlaylist + " added!!", Toast.LENGTH_SHORT).show();
            }
        }
    }

}