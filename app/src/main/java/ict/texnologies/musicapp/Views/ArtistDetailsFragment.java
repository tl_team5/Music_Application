package ict.texnologies.musicapp.Views;

import static android.content.ContentValues.TAG;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import ict.texnologies.musicapp.Adapters.ArtistDetailsAdapter;
import ict.texnologies.musicapp.Adapters.VideoAdapter;
import ict.texnologies.musicapp.Clients.AudioDBClient;
import ict.texnologies.musicapp.Models.ArtistInfo.ArtistInfoList;
import ict.texnologies.musicapp.Models.ArtistInfo.InformationAPI;
import ict.texnologies.musicapp.Models.ArtistInfo.InformationDTO;
import ict.texnologies.musicapp.Models.MusicVideos.MVideoDTO;
import ict.texnologies.musicapp.Models.MusicVideos.MVideoList;
import ict.texnologies.musicapp.Models.MusicVideos.VideoApi;
import ict.texnologies.musicapp.Models.Playlists.Playlist;
import ict.texnologies.musicapp.Models.Playlists.Song;
import ict.texnologies.musicapp.R;
import ict.texnologies.musicapp.ViewModels.SharedViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ArtistDetailsFragment extends Fragment implements VideoAdapter.OnVideoListener {

    Button btnDiscography;
    RecyclerView recyclerView;
    RecyclerView recyclerView2;

    LinearLayoutManager layoutManager;
    LinearLayoutManager layoutManager2;

    VideoAdapter adapter;
    ArtistDetailsAdapter adapter2;

    List<MVideoDTO> videosList = new ArrayList<>();
    List<InformationDTO> infoList = new ArrayList<>();

    private SharedViewModel sharedViewModel;


        public ArtistDetailsFragment() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_artist_details, container, false);
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view,savedInstanceState);

        recyclerView = view.findViewById(R.id.recyclerViewVideos);
        recyclerView2 = view.findViewById(R.id.recyclerViewBiography);
        btnDiscography = view.findViewById(R.id.btnDiscography);

        layoutManager = new LinearLayoutManager(getContext());
        layoutManager2 = new LinearLayoutManager(getContext());

        recyclerView.setLayoutManager(layoutManager);
        recyclerView2.setLayoutManager(layoutManager2);

        adapter2 = new ArtistDetailsAdapter(infoList,getContext());
        adapter = new VideoAdapter(videosList,this,getContext());

        recyclerView.setAdapter(adapter);
        recyclerView2.setAdapter(adapter2);

        Bundle args = getArguments();
        String idArtist = args.getString("id");
        String nameArtist = args.getString("name");
        System.out.println(idArtist);
        System.out.println(nameArtist);

        sharedViewModel = new ViewModelProvider(requireActivity()).get(SharedViewModel.class);


        try {
            fetchVideos(Integer.parseInt(idArtist));
            fetchInformation(nameArtist);
        } catch (IOException e) {
            e.printStackTrace();
        }
        btnDiscography.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Fragment fragment_discography = new DiscographyFragment();
                    fragment_discography.setArguments(args);
                    FragmentTransaction fragmentTransaction = getParentFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.recyclerView, fragment_discography).addToBackStack(null);
                    fragmentTransaction.commit();
            }
        });

    }

    private void fetchVideos(int searchFilter) throws IOException {
        VideoApi videoApi = AudioDBClient.getRetrofitInstance().create(VideoApi.class);
        Call<MVideoList> allVideos = videoApi.getAllVideos(searchFilter);
        allVideos.enqueue(new Callback<MVideoList>() {
            @Override
            public void onResponse(Call<MVideoList> call, Response<MVideoList> response) {
                Log.e("TAG","onResponse:code :"+response.code());
                MVideoList videos = response.body();
                List<MVideoDTO> mVideoDTOS = videos.getMvids();
                videosList.clear();
                for(MVideoDTO mVideoDTO :mVideoDTOS){
                    videosList.add(mVideoDTO);
                    adapter.notifyDataSetChanged();
                }
            }
            @Override
            public void onFailure(Call<MVideoList> call, Throwable t) {
                Log.e("TAG","onResponse:failure :"+t.getMessage());
            }
        });

        }
    private void fetchInformation(String searchFilter) throws IOException {
        InformationAPI informationAPI = AudioDBClient.getRetrofitInstance().create(InformationAPI.class);
        Call<ArtistInfoList> allInfo = informationAPI.getAllInfo(searchFilter);
        allInfo.enqueue(new Callback<ArtistInfoList>() {
            @Override
            public void onResponse(Call<ArtistInfoList> call, Response<ArtistInfoList> response) {
                Log.e("TAG", "onResponse:code :" + response.code());

                ArtistInfoList artistInfoList = response.body();
                List<InformationDTO> information = artistInfoList.getInformation();
                infoList.clear();
                for (InformationDTO informationDTO : information) {
                    System.out.println(informationDTO.getStrBiographyEN());
                    infoList.add(informationDTO);
                    adapter2.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<ArtistInfoList> call, Throwable t) {
                Log.e("TAG", "onResponse:failure :" + t.getMessage());
            }
        });
    }
    @Override
    public void onVideoClick(int position) {
        Log.d(TAG,"onItemClick: clicked");
        Log.d(TAG, String.valueOf(position));


        String musicVid = videosList.get(position).getStrMusicVid();
        String songName = videosList.get(position).getStrTrack();
        ArrayList<Song> songs = new ArrayList<>();
        songs.clear();
        Song song = new Song(songName,"?",musicVid);
        System.out.println(musicVid);
        songs.add(song);
        //args1.putString("musicVid",musicVid);
        //args1.putString("songName",songName);
        sharedViewModel.setPlaylistMutableLiveData(new Playlist("Song",songs));
        Fragment fragment_video_player = new VideoPlayerFragment();
        //fragment_video_player.setArguments(args1);

        getParentFragmentManager().beginTransaction()
                .replace(R.id.recyclerView,fragment_video_player)
                .addToBackStack(null)
                .commit();

    }
}