package ict.texnologies.musicapp.Views;

import android.os.Bundle;
import android.os.StrictMode;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;

import ict.texnologies.musicapp.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.activity_main);
        FragmentManager fragmentManager = getSupportFragmentManager();
        final Fragment fragment1 = new HomeFragment();
        final Fragment fragment2 = new SearchFragment();
        final Fragment fragment3 = new LibraryFragment();
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener(){
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fragment;
                switch (item.getItemId()){
                    case R.id.item_home:
                        fragment=fragment1;
                        break;
                    case R.id.item_search:
                        fragment=fragment2;
                        break;
                    case R.id.item_library:
                        fragment=fragment3;
                        break;
                    default:
                        fragment=fragment3;
                        break;
                }
                fragmentManager.beginTransaction().replace(R.id.recyclerView,fragment).commit();
                return true;
            }
        });




    }
    public void setnavbarVisible() {
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setVisibility(View.VISIBLE);
    }
}