package ict.texnologies.musicapp.Views;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ict.texnologies.musicapp.Adapters.AlbumAdapter;
import ict.texnologies.musicapp.Clients.AudioDBClient;
import ict.texnologies.musicapp.Models.ArtistDiscography.AlbumApi;
import ict.texnologies.musicapp.Models.ArtistDiscography.AlbumDTO;
import ict.texnologies.musicapp.Models.ArtistDiscography.AlbumList;
import ict.texnologies.musicapp.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DiscographyFragment extends Fragment {

    List<AlbumDTO> albumsList;
    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    AlbumAdapter adapter;
    List<AlbumDTO> albumsAList = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //      ViewModel viewModel = new ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory
        //               .getInstance(getActivity().getApplication())).get(AlbumViewModel.class);
        //setup observer, get data

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_discography, container, false);

    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        albumsList = new ArrayList<>();
        adapter = new AlbumAdapter(albumsAList);
        recyclerView.setAdapter(adapter);

        Bundle args = getArguments();
        String idArtist = args.getString("name");
        System.out.println(idArtist+"CHECK");

        try {
            fetchDiscography(idArtist);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void fetchDiscography(String searchFilter) throws IOException {
        AlbumApi albumApi = AudioDBClient.getRetrofitInstance().create(AlbumApi.class);
        Call<AlbumList> allAlbums = albumApi.getAllAlbums(searchFilter);
        allAlbums.enqueue(new Callback<AlbumList>() {
            @Override
            public void onResponse(Call<AlbumList> call, Response<AlbumList> response) {
                Log.e("TAG","onResponse:code :"+response.code());

                AlbumList albumList = response.body();
                List<AlbumDTO> albums = albumList.getAlbum();
                if(albums !=null) {
                    for (AlbumDTO albumDTO : albums) {

                        System.out.println(albumDTO.getStrAlbum());
                        System.out.println(albumDTO.getIntYearReleased());
                        albumsAList.add(albumDTO);
                        adapter.notifyDataSetChanged();
                    }
                }

            }

            @Override
            public void onFailure(Call<AlbumList> call, Throwable t) {
                Log.e("TAG","onResponse:failure :"+t.getMessage());
            }
        });
    }


}