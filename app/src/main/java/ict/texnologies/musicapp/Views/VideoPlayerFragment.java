package ict.texnologies.musicapp.Views;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.utils.YouTubePlayerUtils;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;

import java.util.ArrayList;

import ict.texnologies.musicapp.Models.Playlists.Playlist;
import ict.texnologies.musicapp.Models.Playlists.Song;
import ict.texnologies.musicapp.R;
import ict.texnologies.musicapp.ViewModels.SharedViewModel;


public class VideoPlayerFragment extends Fragment {
    private YouTubePlayerView youTubePlayerView;
    Playlist playlist_rec;
    private SharedViewModel sharedViewModel;
    private VideoIdsProvider videoIdsProvider;

    public VideoPlayerFragment() {
    }
    Observer<Playlist> addedPlaylistObserver= new Observer<Playlist>() {
        @Override
        public void onChanged(Playlist playlist) {
            playlist_rec=playlist;
            System.out.println(playlist.getSongs());
        }
    };
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video_player, container, false);
        youTubePlayerView = view.findViewById(R.id.youtube_player_view);

        initYouTubePlayerView(view);

        return view;    }

    @Override
    public void onViewCreated(@NonNull View view, @androidx.annotation.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view,savedInstanceState);


        videoIdsProvider= new VideoIdsProvider();
        videoIdsProvider.clearvids();
        sharedViewModel = new ViewModelProvider(requireActivity()).get(SharedViewModel.class);
        sharedViewModel.getPlaylistMutableLiveData().observe(requireActivity(),addedPlaylistObserver);
        for (Song song : playlist_rec.getSongs())
        {
            System.out.println(song.getSongUrl());
            if (song.getSongUrl().indexOf("s")==4) {
                videoIdsProvider.addVid(song.getSongUrl().substring(32));
                System.out.println(videoIdsProvider.getVideoId());
            }
            else {
                videoIdsProvider.addVid(song.getSongUrl().substring(31));
                System.out.println(videoIdsProvider.getVideoId());

            }
        }
    }

    private void initYouTubePlayerView(View view) {
        youTubePlayerView.setEnableAutomaticInitialization(false);
        youTubePlayerView.initialize(new AbstractYouTubePlayerListener() {
            @Override
            public void onStateChange(@NonNull YouTubePlayer youTubePlayer, @NonNull PlayerConstants.PlayerState state) {
                super.onStateChange(youTubePlayer, state);
                if (state == PlayerConstants.PlayerState.ENDED)
                {
                    YouTubePlayerUtils.loadOrCueVideo(
                            youTubePlayer, getLifecycle(),
                            videoIdsProvider.getNextVideoId(),0f
                    );
                }
            }

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onReady(@NonNull YouTubePlayer youTubePlayer) {
                super.onReady(youTubePlayer);

                    YouTubePlayerUtils.loadOrCueVideo(
                            youTubePlayer, getLifecycle(),
                            videoIdsProvider.getNextVideoId(),0f
                    );
                Button play_next = view.findViewById(R.id.play_next);
                play_next.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        YouTubePlayerUtils.loadOrCueVideo(
                                youTubePlayer, getLifecycle(),
                                videoIdsProvider.getNextVideoId(),0f
                        );
                    }
                });
                Button play_prev = view.findViewById(R.id.play_prev);
                play_prev.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        YouTubePlayerUtils.loadOrCueVideo(
                                youTubePlayer, getLifecycle(),
                                videoIdsProvider.getPreviousVideoID(),0f
                        );
                    }
                });

            }

        });

    }


}
 class VideoIdsProvider {
    private static ArrayList<String> videoIds ;
    private int currentPlaying=-1;
    public void clearvids(){videoIds=null;}
    public String getNextVideoId() {
        currentPlaying+=1;
        return videoIds.get(currentPlaying);
    }
     public String getVideoId() {

         return videoIds.get(0);
     }
    public String getPreviousVideoID(){
        currentPlaying-=1;
        return videoIds.get(currentPlaying);
    }
    public void addVid(String added)
    {
        if (videoIds==null)
            videoIds=new ArrayList<>();
        videoIds.add(added);
    }
    public void setVidID(ArrayList<String> VIDS) {
       videoIds=VIDS;
    }
}