package ict.texnologies.musicapp.Views;

import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import ict.texnologies.musicapp.Adapters.TopArtistsAdapter;
import ict.texnologies.musicapp.Clients.LastFMClient;
import ict.texnologies.musicapp.Models.TopArtists.TopArtistsAPI;
import ict.texnologies.musicapp.Models.TopArtists.TopArtistsDTO;
import ict.texnologies.musicapp.Models.TopArtists.VeryTopArtists;
import ict.texnologies.musicapp.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TopArtistsFragment extends Fragment implements TopArtistsAdapter.OnArtistsListener {

    Button buttonGetTopArtists;
    LinearLayoutManager layoutManager;
    RecyclerView recyclerView;
    TopArtistsAdapter adapter;
    List<TopArtistsDTO> TheArtistList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_top_artists, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.TArecyclerView);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new TopArtistsAdapter(TheArtistList, this, getContext());
        recyclerView.setAdapter(adapter);

        buttonGetTopArtists = view.findViewById(R.id.buttonGetArtists);
        buttonGetTopArtists.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchTopArtists();
            }
        });
    }

    private void fetchTopArtists() {
        TopArtistsAPI topArtistsAPI = LastFMClient.getRetrofitInstance().create(TopArtistsAPI.class);
        Call<VeryTopArtists> call = topArtistsAPI.getAllTopArtists();
        call.enqueue(new Callback<VeryTopArtists>() {
            @Override
            public void onResponse(Call<VeryTopArtists> call, Response<VeryTopArtists> response) {
                Log.e("TAG", "onResponse:code :" + response.code());

                for (int i = 0; i < response.body().getTopArtists().getArtist().size(); i++) {
                    System.out.println(response.body().getTopArtists().getArtist().get(i).getName());
                    TheArtistList.add(response.body().getTopArtists().getArtist().get(i));
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<VeryTopArtists> call, Throwable t) {
                Log.e("TAG", "onResponse:failure :" + t.getMessage().toString());
            }

        });
    }

    @Override
    public void onArtistsClick(int position) {

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(TheArtistList.get(position).getUrl()));

        startActivity(browserIntent);
    }
}
