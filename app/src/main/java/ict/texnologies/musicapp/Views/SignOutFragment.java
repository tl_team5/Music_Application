package ict.texnologies.musicapp.Views;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import ict.texnologies.musicapp.R;
import ict.texnologies.musicapp.ViewModels.AuthViewModel;

public class SignOutFragment extends Fragment {
    private AuthViewModel viewModel;
    private NavController navController;
    private Button signOutBtn;
    private Button getDisc;
    private Button getSongs;
    private Button getInfo;
    private FloatingActionButton addPlaylistsBtn;
    private Button topAlbumsButton;
    private Button buttonGetTopArtists;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory
                .getInstance(getActivity().getApplication())).get(AuthViewModel.class);
        viewModel.getLoggedStatus().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if (aBoolean) {
                    navController.navigate(R.id.action_signOutFragment_to_signInFragment);
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_out, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        navController = Navigation.findNavController(view);
        signOutBtn = view.findViewById(R.id.signOutBtn);
        getDisc = view.findViewById(R.id.buttonGetDiscographyMain);
        getSongs = view.findViewById(R.id.buttonGetTracksMain);
        getInfo = view.findViewById(R.id.buttonGetInfoMain);
        addPlaylistsBtn = view.findViewById(R.id.addPlaylistsBtn);
        buttonGetTopArtists = view.findViewById(R.id.buttonGetTopArtists);
        topAlbumsButton = view.findViewById(R.id.topAlbumsBtn);

        ((MainActivity) getActivity()).setnavbarVisible();
        signOutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.signOut();
            }
        });
        getDisc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.action_signOutFragment_to_discographyFragment);
            }
        });
        getSongs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.action_signOutFragment_to_trackFragment);
            }
        });
        getInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.action_signOutFragment_to_infoFragment);
            }
        });
        addPlaylistsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nameFragment = "SignOutFragment";
                SignOutFragmentDirections.ActionSignOutFragmentToAddPlaylistsFragment action = SignOutFragmentDirections.actionSignOutFragmentToAddPlaylistsFragment("","","",nameFragment);
                navController.navigate(action);
            }
        });
        buttonGetTopArtists.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.action_signOutFragment_to_topArtistsFragment);
            }
        });

        topAlbumsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navController.navigate(R.id.action_signOutFragment_to_topAlbumsFragment);
            }
        });
    }
}