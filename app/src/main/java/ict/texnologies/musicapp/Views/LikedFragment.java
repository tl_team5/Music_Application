package ict.texnologies.musicapp.Views;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import ict.texnologies.musicapp.Adapters.LikedArtistsAdapter;
import ict.texnologies.musicapp.R;

public class LikedFragment extends Fragment implements LikedArtistsAdapter.OnArtistsListener{
    LinearLayoutManager likedArtistsLayout;
    RecyclerView likedArtistsRecycler;
    LikedArtistsAdapter likedArtistsAdapter;
    List<String> TheArtistList = new ArrayList<>();

    public LikedFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        likedArtistsLayout=new LinearLayoutManager(getContext());
        likedArtistsRecycler=view.findViewById(R.id.liked_artists_recycler);
        likedArtistsRecycler.setLayoutManager(likedArtistsLayout);
        likedArtistsAdapter =new LikedArtistsAdapter(TheArtistList,this,getContext());
        likedArtistsRecycler.setAdapter(likedArtistsAdapter);
        FirebaseDatabase database = FirebaseDatabase.getInstance("https://music-app-2604f-default-rtdb.europe-west1.firebasedatabase.app");
        DatabaseReference myRef = database.getReference();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        String userID = user.getUid();
        ValueEventListener valueEventListener= new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                TheArtistList.clear();
                for (DataSnapshot snapshot : dataSnapshot.child("users").child(userID).child("fav_artists").getChildren()) {
                    TheArtistList.add(snapshot.getKey());
                }
                likedArtistsAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        };
        myRef.addValueEventListener(valueEventListener);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_liked, container, false);
    }

    @Override
    public void onArtistsClick(int position) {

    }
}