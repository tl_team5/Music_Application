package ict.texnologies.musicapp.Views;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ict.texnologies.musicapp.Adapters.ArtistSearchAdapter;
import ict.texnologies.musicapp.Adapters.PlaylistsRecyclerViewAdapter;
import ict.texnologies.musicapp.Adapters.SearchAlbumAdapter;
import ict.texnologies.musicapp.Adapters.SearchTrackAdapter;
import ict.texnologies.musicapp.Clients.AudioDBClient;
import ict.texnologies.musicapp.Clients.LastFMClient;
import ict.texnologies.musicapp.Models.AlbumSearch.SearchAlbumAPI;
import ict.texnologies.musicapp.Models.AlbumSearch.SearchAlbumDTO;
import ict.texnologies.musicapp.Models.ArtistInfo.ArtistInfoList;
import ict.texnologies.musicapp.Models.ArtistInfo.InformationAPI;
import ict.texnologies.musicapp.Models.ArtistInfo.InformationDTO;
import ict.texnologies.musicapp.Models.ArtistSearch.Search;
import ict.texnologies.musicapp.Models.ArtistSearch.SearchTrackAPI;
import ict.texnologies.musicapp.Models.ArtistSearch.SearchTrackDTO;
import ict.texnologies.musicapp.Models.Playlists.Playlist;
import ict.texnologies.musicapp.Models.Playlists.Song;
import ict.texnologies.musicapp.R;
import ict.texnologies.musicapp.ViewModels.SharedViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchFragment extends Fragment implements ArtistSearchAdapter.OnItemListener,SearchTrackAdapter.OnTrackListener,PlaylistsRecyclerViewAdapter.OnPlaylistListener {

    TextView textViewSearch;
    RecyclerView recyclerViewTracks;
    RecyclerView recyclerViewArtists;
    RecyclerView recyclerViewAlbums;
    RecyclerView recyclerPlaylists;

    LinearLayoutManager layoutManagerTracks;
    LinearLayoutManager layoutManagerArtists;
    LinearLayoutManager layoutManagerAlbums;
    LinearLayoutManager layoutManagerPlaylists;

    List<InformationDTO> artistList = new ArrayList<>();
    List<SearchTrackDTO> trackList = new ArrayList<>();
    List<SearchAlbumDTO> albumList = new ArrayList<>();
    ArrayList<Playlist> playlistList = new ArrayList<>();

    ArtistSearchAdapter artistAdapter;
    SearchTrackAdapter trackAdapter;
    SearchAlbumAdapter albumAdapter;
    PlaylistsRecyclerViewAdapter playlistAdapter;

    ImageView imageView;
    private SharedViewModel sharedViewModel;

    // for the playlists
    FirebaseDatabase database = FirebaseDatabase.getInstance("https://music-app-2604f-default-rtdb.europe-west1.firebasedatabase.app");
    DatabaseReference myRef = database.getReference();
    String userID;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        userID = user.getUid();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        sharedViewModel = new ViewModelProvider(requireActivity()).get(SharedViewModel.class);
        textViewSearch = view.findViewById(R.id.textViewSearch);
        imageView = view.findViewById(R.id.search_imageView);

        recyclerViewArtists = view.findViewById(R.id.recycler_artists);
        recyclerViewTracks = view.findViewById(R.id.recycler_tracks);
        recyclerViewAlbums = view.findViewById(R.id.recycler_albums);
        recyclerPlaylists = view.findViewById(R.id.recycler_playlists);

        layoutManagerArtists = new LinearLayoutManager(getContext());
        layoutManagerTracks = new LinearLayoutManager(getContext());
        layoutManagerAlbums = new LinearLayoutManager(getContext());
        layoutManagerPlaylists = new LinearLayoutManager(getContext());

        recyclerViewArtists.setLayoutManager(layoutManagerArtists);
        recyclerViewTracks.setLayoutManager(layoutManagerTracks);
        recyclerViewAlbums.setLayoutManager(layoutManagerAlbums);
        recyclerPlaylists.setLayoutManager(layoutManagerPlaylists);

        artistAdapter = new ArtistSearchAdapter(artistList,this);
        trackAdapter = new SearchTrackAdapter(trackList,this,getContext());
        albumAdapter = new SearchAlbumAdapter(albumList);
        playlistAdapter = new PlaylistsRecyclerViewAdapter(playlistList, SearchFragment.this);

        recyclerViewArtists.setAdapter(artistAdapter);
        recyclerViewTracks.setAdapter(trackAdapter);
        recyclerViewAlbums.setAdapter(albumAdapter);
        recyclerPlaylists.setAdapter(playlistAdapter);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!textViewSearch.getText().toString().isEmpty()) {
                    try {
                        fetchInformation(textViewSearch.getText().toString());
                        fetchSearchedTracks(textViewSearch.getText().toString());
                        fetchSearchedAlbums(textViewSearch.getText().toString());
                        fetchPlaylists();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
    private void fetchInformation(String searchFilter) throws IOException {
        InformationAPI informationAPI = AudioDBClient.getRetrofitInstance().create(InformationAPI.class);
        Call<ArtistInfoList> allInfo = informationAPI.getAllInfo(searchFilter);
        allInfo.enqueue(new Callback<ArtistInfoList>() {
            @Override
            public void onResponse(Call<ArtistInfoList> call, Response<ArtistInfoList> response) {
//                Log.e("TAG","onResponse:code :"+response.code());
                artistList.clear();
                ArtistInfoList artistInfoList = response.body();
                List<InformationDTO> information = artistInfoList.getInformation();
                if(information !=null) {
                    for (InformationDTO informationDTO : information) {
//                        System.out.println(informationDTO.getStrBiographyEN());
                        artistList.add(informationDTO);
                        artistAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<ArtistInfoList> call, Throwable t) {
                Log.e("TAG","onResponse:failure :"+t.getMessage());
            }
        });
    }


    private void fetchSearchedTracks(String searchFilter) throws IOException {
        SearchTrackAPI searchTrackAPI = LastFMClient.getRetrofitInstance().create(SearchTrackAPI.class);
        Call<Search> searchCall = searchTrackAPI.getSearchTrack(searchFilter);
        searchCall.enqueue(new Callback<Search>() {
            @Override
            public void onResponse(Call<Search> call, Response<Search> response) {
                trackList.clear();
                if(response.body()!= null) {
                    for (int i = 0; i < response.body().getResults().searchTracksList().getTracks().size(); i++) {
//                        System.out.println(response.body().getResults().searchTracksList().getTracks().get(i).getName());
                        trackList.add(response.body().getResults().searchTracksList().getTracks().get(i));
                        trackAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(Call<Search> call, Throwable t) {
                Log.e("TAG","onResponse:failure :"+t.getMessage());
            }
        });
    }

    private void fetchSearchedAlbums(String searchFilter) throws IOException {
        SearchAlbumAPI searchAlbumAPI = LastFMClient.getRetrofitInstance().create(SearchAlbumAPI.class);
        Call<ict.texnologies.musicapp.Models.AlbumSearch.Search> searchCall = searchAlbumAPI.getSearchAlbum(searchFilter);
        searchCall.enqueue(new Callback<ict.texnologies.musicapp.Models.AlbumSearch.Search>() {
            @Override
            public void onResponse(Call<ict.texnologies.musicapp.Models.AlbumSearch.Search> call, Response<ict.texnologies.musicapp.Models.AlbumSearch.Search> response) {
                albumList.clear();
                if (response.body() != null ){
                    for (int i = 0; i < response.body().getResults().searchAlbumList().getAlbum().size(); i++) {
//                        System.out.println(response.body().getResults().getSearchAlbumList().getAlbum().get(i).getName());
                        albumList.add(response.body().getResults().searchAlbumList().getAlbum().get(i));
                        albumAdapter.notifyDataSetChanged();
                    }
            }
            }

            @Override
            public void onFailure(Call<ict.texnologies.musicapp.Models.AlbumSearch.Search> call, Throwable t) {
                Log.e("TAG","onResponse:failure :"+t.getMessage());
            }
        });
    }
    private void fetchPlaylists() throws IOException{
        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                playlistList.clear();
                for (DataSnapshot snapshot: dataSnapshot.child("public_playlists").getChildren()){
                    if (snapshot.hasChildren()) {
                    for (DataSnapshot playlist:snapshot.getChildren()) {
                        String plname= (String) playlist.child("playlist").getValue();
                        String plUserID= (String) playlist.child("userID").getValue();
                        if(plname.equals(textViewSearch.getText().toString())) {
                            ArrayList<Song> songstemp = new ArrayList<>();
                            songstemp.clear();
                            for (DataSnapshot song:dataSnapshot.child("users").child(plUserID).child("playlists").child(plname).getChildren())
                                songstemp.add(new Song(song.child("nameOfSong").getValue().toString(),song.child("nameOfSinger").getValue().toString(),song.child("songUrl").getValue().toString()));
                            playlistList.add(new Playlist(plname,songstemp));

                        }
                    }
                        playlistAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                System.out.println("Error reading playlist from db");

            }

        };
        myRef.addValueEventListener(valueEventListener);

    }

    @Override
    public void onItemClick(int position) {
        Bundle args = new Bundle();
        String idArtist = artistList.get(position).getIdArtist();
        String nameArtist = artistList.get(position).getStrArtist();

        args.putString("id",idArtist);
        args.putString("name",nameArtist);

        Fragment fragment_artist_details = new ArtistDetailsFragment();
        fragment_artist_details.setArguments(args);

        getParentFragmentManager().beginTransaction()
                .replace(R.id.recyclerView,fragment_artist_details)
                .addToBackStack(null)
                .commit();

    }

    @Override
    public void onTrackClick(int position) {
        System.out.println("Hello" );
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(trackList.get(position).getUrl()));
        startActivity(browserIntent);
    }

    @Override
    public void onTrackLongClick(int position) {

    }

    @Override
    public void onPlaylistClick(int position) {
        sharedViewModel.setPlaylistMutableLiveData(new Playlist(playlistList.get(position).getNameOfPlaylist(), playlistList.get(position).getSongs()));
        Fragment fragment_addsongs = new AddSongsFragment();
        FragmentTransaction fragmentTransaction = getParentFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.recyclerView, fragment_addsongs).addToBackStack(null);
        fragmentTransaction.commit();
    }
}