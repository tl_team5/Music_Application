package ict.texnologies.musicapp.Views;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import ict.texnologies.musicapp.Adapters.TrackAdapter;
import ict.texnologies.musicapp.Clients.LastFMClient;
import ict.texnologies.musicapp.Models.Tracks.TopTracks;
import ict.texnologies.musicapp.Models.Tracks.TrackApi;
import ict.texnologies.musicapp.Models.Tracks.TrackDTO;
import ict.texnologies.musicapp.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TrackFragment extends Fragment implements TrackAdapter.OnTrackListener{

    Button buttonGetTracks;
    LinearLayoutManager layoutManager;
    RecyclerView recyclerView;
    TrackAdapter adapter;
    List<TrackDTO> trackAList = new ArrayList<>();
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_track, container, false);

    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view,savedInstanceState);

        recyclerView = view.findViewById(R.id.trackRecyclerView);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new TrackAdapter(trackAList,this,getContext());
        recyclerView.setAdapter(adapter);


        buttonGetTracks = view.findViewById(R.id.buttonGetTracks);
        buttonGetTracks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchTracks();
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        menu.clear();
        inflater.inflate(R.menu.search_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.actionSearch);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                // inside on query text change method we are
                // calling a method to filter our recycler view.
                filter(newText);
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void fetchTracks() {
        TrackApi trackApi = LastFMClient.getRetrofitInstance().create(TrackApi.class);
        Call<TopTracks> call = trackApi.getAllTracks();
        call.enqueue(new Callback<TopTracks>() {
            @Override
            public void onResponse(Call<TopTracks> call, Response<TopTracks> response) {
                Log.e("TAG","onResponse:code :"+response.code());

                trackAList.clear();
                for(int i=0; i<response.body().getTracks().getTrack().size() ;i++){
                    System.out.println(response.body().getTracks().getTrack().get(i).getName());
                    trackAList.add(response.body().getTracks().getTrack().get(i));
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<TopTracks> call, Throwable t) {
                Log.e("TAG","onResponse:failure :"+t.getMessage().toString());
            }
        });
    }

    @Override
    public void onTrackClick(int position) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(trackAList.get(position).getUrl()));
        startActivity(browserIntent);
    }

    @Override
    public void onTrackLongClick(int position) {

    }

    private void filter(String text) {
        // creating a new array list to filter our data.
        List<TrackDTO> filteredlist = new ArrayList<>();

        // running a for loop to compare elements.
        for (TrackDTO item : trackAList) {
            // checking if the entered string matched with any item of our recycler view.
            if (item.getName().toLowerCase().contains(text.toLowerCase())) {
                // if the item is matched we are
                // adding it to our filtered list.
                filteredlist.add(item);
            }
        }
        if (filteredlist.isEmpty()) {
            // if no item is added in filtered list we are
            // displaying a toast message as no data found.
            Toast.makeText(getContext(), "No Data Found..", Toast.LENGTH_SHORT).show();
        } else {
            // at last we are passing that filtered
            // list to our adapter class.
            adapter.filterList(filteredlist);
        }
    }
}