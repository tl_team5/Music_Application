package ict.texnologies.musicapp.Views;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

import ict.texnologies.musicapp.Adapters.TopAlbumsAdapter;
import ict.texnologies.musicapp.Clients.AudioDBClient;
import ict.texnologies.musicapp.Models.ArtistInfo.InformationDTO;
import ict.texnologies.musicapp.Models.MostLovedAlbums.LovedAlbumsDTO;
import ict.texnologies.musicapp.Models.MostLovedAlbums.LovedAlbumsList;
import ict.texnologies.musicapp.Models.MostLovedAlbums.MostLovedAlbumsAPI;
import ict.texnologies.musicapp.Models.MusicVideos.MVideoDTO;
import ict.texnologies.musicapp.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TopAlbumsFragment extends Fragment {

    Button buttonTopAlbums;
    List<LovedAlbumsDTO> lovedAlbumsDTOList = new ArrayList<>();
    LinearLayoutManager layoutManager;
    RecyclerView recyclerView;
    TopAlbumsAdapter adapter;


   public TopAlbumsFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_top_albums, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState){
        recyclerView = view.findViewById(R.id.topAlbumsRecyclerView);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new TopAlbumsAdapter(lovedAlbumsDTOList);
        recyclerView.setAdapter(adapter);


       buttonTopAlbums = view.findViewById(R.id.topAlbumsFetchBtn);
       buttonTopAlbums.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               try {
                   fetchTopAlbums();
               } catch (IOException e) {
                   e.printStackTrace();
               }
           }
       });
    }
    /*
    random stuff
     */

    private void fetchTopAlbums() throws IOException {
        MostLovedAlbumsAPI mostLovedAlbumsAPI = AudioDBClient.getRetrofitInstance().create(MostLovedAlbumsAPI.class);
        Call<LovedAlbumsList> lovedAlbumsListCall = mostLovedAlbumsAPI.getAllInfo();
        lovedAlbumsListCall.enqueue(new Callback<LovedAlbumsList>() {
            @Override
            public void onResponse(Call<LovedAlbumsList> call, Response<LovedAlbumsList> response) {
                Log.e("TAG", "onResponse:code :"+response.code());
                LovedAlbumsList lovedAlbumsList = response.body();
                List<LovedAlbumsDTO> lovedAlbumsDTOS = lovedAlbumsList.getLovedAlbumsDTO();
                for(LovedAlbumsDTO lovedAlbumsDTO : lovedAlbumsDTOS){
                    System.out.println(lovedAlbumsDTO.getStrAlbum());
                    System.out.println(lovedAlbumsDTO.getStrArtistStripped());
                    lovedAlbumsDTOList.add(lovedAlbumsDTO);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<LovedAlbumsList> call, Throwable t) {
                Log.e("TAG", "onResponse:failure :" + t.getMessage());
            }
        });
        }
}