package ict.texnologies.musicapp.Views;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.PopupMenu;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ict.texnologies.musicapp.Adapters.TopAlbumsAdapter;
import ict.texnologies.musicapp.Adapters.TopArtistsAdapter;
import ict.texnologies.musicapp.Adapters.TrackAdapter;
import ict.texnologies.musicapp.Clients.AudioDBClient;
import ict.texnologies.musicapp.Clients.LastFMClient;
import ict.texnologies.musicapp.Models.MostLovedAlbums.LovedAlbumsDTO;
import ict.texnologies.musicapp.Models.MostLovedAlbums.LovedAlbumsList;
import ict.texnologies.musicapp.Models.MostLovedAlbums.MostLovedAlbumsAPI;
import ict.texnologies.musicapp.Models.Playlists.Song;
import ict.texnologies.musicapp.Models.TopArtists.TopArtistsAPI;
import ict.texnologies.musicapp.Models.TopArtists.TopArtistsDTO;
import ict.texnologies.musicapp.Models.TopArtists.VeryTopArtists;
import ict.texnologies.musicapp.Models.Tracks.TopTracks;
import ict.texnologies.musicapp.Models.Tracks.TrackApi;
import ict.texnologies.musicapp.Models.Tracks.TrackDTO;
import ict.texnologies.musicapp.R;
import ict.texnologies.musicapp.ViewModels.SharedViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment  implements TrackAdapter.OnTrackListener,TopArtistsAdapter.OnArtistsListener, PopupMenu.OnMenuItemClickListener {

    LinearLayoutManager topSongsLayout,topArtistsLayout,topAlbumsLayout;
    RecyclerView topSongsRecycler,topArtistsRecycler,topAlbumsRecycler;
    TrackAdapter songsAdapter;
    TopArtistsAdapter topArtistsAdapter;
    TopAlbumsAdapter topAlbumsAdapter;
    List<TrackDTO> trackAList = new ArrayList<>();
    List<TopArtistsDTO> TheArtistList = new ArrayList<>();
    List<LovedAlbumsDTO> lovedAlbumsDTOList = new ArrayList<>();
    int songsAdapterpos;
    private SharedViewModel sharedViewModel;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ((MainActivity) getActivity()).setnavbarVisible();
        sharedViewModel = new ViewModelProvider(requireActivity()).get(SharedViewModel.class);

        topSongsLayout= new LinearLayoutManager(getContext());
        topArtistsLayout=new LinearLayoutManager(getContext());
        topAlbumsLayout=new LinearLayoutManager(getContext());

        topSongsRecycler=view.findViewById(R.id.top_songs_recycler);
        topArtistsRecycler=view.findViewById(R.id.top_artists_recycler);
        topAlbumsRecycler=view.findViewById(R.id.top_albums_recycler);

        topSongsRecycler.setLayoutManager(topSongsLayout);
        topArtistsRecycler.setLayoutManager(topArtistsLayout);
        topAlbumsRecycler.setLayoutManager(topAlbumsLayout);

        songsAdapter=new TrackAdapter(trackAList,this,getContext());
        topArtistsAdapter =new TopArtistsAdapter(TheArtistList,this,getContext());
        topAlbumsAdapter=new TopAlbumsAdapter(lovedAlbumsDTOList);

        topSongsRecycler.setAdapter(songsAdapter);
        topArtistsRecycler.setAdapter(topArtistsAdapter);
        topAlbumsRecycler.setAdapter(topAlbumsAdapter);


        fetchTracks();
        fetchTopArtists();
        fetchTopAlbums();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    private void fetchTracks() {
        TrackApi trackApi = LastFMClient.getRetrofitInstance().create(TrackApi.class);
        Call<TopTracks> call = trackApi.getAllTracks();
        call.enqueue(new Callback<TopTracks>() {
            @Override
            public void onResponse(Call<TopTracks> call, Response<TopTracks> response) {
                trackAList.clear();
                for(int i=0; i<response.body().getTracks().getTrack().size() ;i++){
//                    System.out.println(response.body().getTracks().getTrack().get(i).getName());
                    trackAList.add(response.body().getTracks().getTrack().get(i));
                    songsAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<TopTracks> call, Throwable t) {
                Log.e("TAG","Top Tracks onResponse:failure :"+t.getMessage());
            }
        });
    }

    private void fetchTopArtists() {
        TopArtistsAPI topArtistsAPI = LastFMClient.getRetrofitInstance().create(TopArtistsAPI.class);
        Call<VeryTopArtists> call = topArtistsAPI.getAllTopArtists();
        call.enqueue(new Callback<VeryTopArtists>() {
            @Override
            public void onResponse(Call<VeryTopArtists> call, Response<VeryTopArtists> response) {
                TheArtistList.clear();
                for (int i = 0; i < response.body().getTopArtists().getArtist().size(); i++) {
//                    System.out.println(response.body().getTopArtists().getArtist().get(i).getName());
                    TheArtistList.add(response.body().getTopArtists().getArtist().get(i));
                    topArtistsAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<VeryTopArtists> call, Throwable t) {
                Log.e("TAG", "Top Artists onResponse:failure :" + t.getMessage());
            }

        });
    }
    private void fetchTopAlbums() {
        MostLovedAlbumsAPI mostLovedAlbumsAPI = AudioDBClient.getRetrofitInstance().create(MostLovedAlbumsAPI.class);
        Call<LovedAlbumsList> lovedAlbumsListCall = mostLovedAlbumsAPI.getAllInfo();
        lovedAlbumsListCall.enqueue(new Callback<LovedAlbumsList>() {
            @Override
            public void onResponse(Call<LovedAlbumsList> call, Response<LovedAlbumsList> response) {
                lovedAlbumsDTOList.clear();
                LovedAlbumsList lovedAlbumsList = response.body();
                List<LovedAlbumsDTO> lovedAlbumsDTOS = lovedAlbumsList.getLovedAlbumsDTO();
                for(LovedAlbumsDTO lovedAlbumsDTO : lovedAlbumsDTOS){
                    lovedAlbumsDTOList.add(lovedAlbumsDTO);
                    topAlbumsAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<LovedAlbumsList> call, Throwable t) {
                Log.e("TAG", "Top Albums onResponse:failure :" + t.getMessage());
            }
        });
    }
    @Override
    public void onTrackClick(int position) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(trackAList.get(position).getUrl()));
        startActivity(browserIntent);
    }

    @Override
    public void onTrackLongClick(int position) {
        songsAdapterpos=position;
        showPopupMenu(topSongsRecycler.findViewHolderForAdapterPosition(position).itemView);
    }

    @Override
    public void onArtistsClick(int position) {

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(TheArtistList.get(position).getUrl()));

        startActivity(browserIntent);
    }
    private void showPopupMenu(View view) {
        PopupMenu popupMenu = new PopupMenu(view.getContext(),view);
        popupMenu.inflate(R.menu.recycler_popup_menu);
        popupMenu.setOnMenuItemClickListener(this);
        popupMenu.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_addtoplaylist:
                String href = null;
                Document doc = null;
                try {
                    doc = Jsoup.connect(trackAList.get(songsAdapterpos).getUrl()).get();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Elements links = doc.select("#track-page-video-playlink");

                for (Element link : links) {
                    href = link.attr("href");

                }
                Song song= new Song(trackAList.get(songsAdapterpos).getName()
                        ,trackAList.get(songsAdapterpos).getArtist().getName()
                        ,href
                        ,trackAList.get(songsAdapterpos).getMbid());

                new AddPlaylistDialog(song).show(getChildFragmentManager(),"Add playlist");
                sharedViewModel.setSongMutableLiveData(song);
                return true;
            default:
                return false;
        }
    }
}