package ict.texnologies.musicapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

import ict.texnologies.musicapp.R;

public class LikedArtistsAdapter extends RecyclerView.Adapter<LikedArtistsAdapter.ViewHolder> {
    private List<String> topArtistsDTOList;
    private LikedArtistsAdapter.OnArtistsListener mOnItemListener;
    FirebaseDatabase database = FirebaseDatabase.getInstance("https://music-app-2604f-default-rtdb.europe-west1.firebasedatabase.app");
    DatabaseReference myRef = database.getReference();
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    String userID = user.getUid();

    private LikedArtistsAdapter.OnArtistsListener TAOnArtistsListener;
    Context context;
    public LikedArtistsAdapter(List<String> topArtistsDTOList, LikedArtistsAdapter.OnArtistsListener onArtistsListener, Context context) {
        this.topArtistsDTOList = topArtistsDTOList;
        this.TAOnArtistsListener = onArtistsListener;
        this.context = context;
    }

    @NonNull
    @Override
    public LikedArtistsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.topartist_item, parent, false);
        return new LikedArtistsAdapter.ViewHolder(view,TAOnArtistsListener);
    }
    @Override
    public void onBindViewHolder(@NonNull LikedArtistsAdapter.ViewHolder holder, int position) {
        holder.tvTopArtistsName.setText(topArtistsDTOList.get(position));

    }
    @Override
    public int getItemCount() {
        return topArtistsDTOList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView tvTopArtistsName;
        ImageView ivTopArtists;
        LikedArtistsAdapter.OnArtistsListener onArtistsListener;
        TextView tvFavButton;
        public ViewHolder(@NonNull View itemView, LikedArtistsAdapter.OnArtistsListener onArtistsListener) {
            super(itemView);
            tvTopArtistsName = itemView.findViewById(R.id.textViewTopArtistsName);
            ivTopArtists = itemView.findViewById(R.id.imageViewTopArtists);
            this.onArtistsListener = onArtistsListener;
            itemView.setOnClickListener(this);

            tvFavButton = itemView.findViewById(R.id.favBtn);
            itemView.setOnClickListener(this);
            tvFavButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                }
            });
        }
        @Override
        public void onClick(View view){
            onArtistsListener.onArtistsClick(getAdapterPosition());
        }
    }
    public interface OnArtistsListener{
        void onArtistsClick(int position);
    }
}