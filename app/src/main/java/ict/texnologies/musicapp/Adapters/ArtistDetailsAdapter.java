package ict.texnologies.musicapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import ict.texnologies.musicapp.Models.ArtistInfo.InformationDTO;
import ict.texnologies.musicapp.R;

public class ArtistDetailsAdapter extends RecyclerView.Adapter<ArtistDetailsAdapter.ViewHolder> {

    private List<InformationDTO> ArtistInfoList;
    Context context;
    public ArtistDetailsAdapter(List<InformationDTO> artistInfoList, Context context) {
        this.ArtistInfoList = artistInfoList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.info_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.tvstrArtist.setText(ArtistInfoList.get(position).getStrArtist());
        holder.tvstrBiographyEN.setText(ArtistInfoList.get(position).getStrBiographyEN());
        Glide.with(this.context)
                .load(ArtistInfoList.get(position).getStrArtistLogo())
                .into(holder.ivArtist);


    }

    @Override
    public int getItemCount() {
        return ArtistInfoList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

            TextView tvstrArtist;
            TextView tvstrBiographyEN;
            ImageView ivArtist;
            public ViewHolder(@NonNull View itemView) {
                super(itemView);

                tvstrArtist = itemView.findViewById(R.id.tvArtistName);
                tvstrBiographyEN = itemView.findViewById(R.id.tvArtistBio);
                ivArtist = itemView.findViewById(R.id.imageViewArtistLogo);

            }
        }
}
