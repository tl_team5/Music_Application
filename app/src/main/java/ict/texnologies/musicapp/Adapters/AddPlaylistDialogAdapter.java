package ict.texnologies.musicapp.Adapters;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import ict.texnologies.musicapp.Models.Playlists.Playlist;
import ict.texnologies.musicapp.R;

public class AddPlaylistDialogAdapter extends RecyclerView.Adapter<AddPlaylistDialogAdapter.ViewHolder> {

    private AddPlaylistDialogAdapter.OnPlaylistListener mOnPlaylistListener;
    private ArrayList<Playlist> playlists = new ArrayList<>();
    FirebaseDatabase database = FirebaseDatabase.getInstance("https://music-app-2604f-default-rtdb.europe-west1.firebasedatabase.app");
    DatabaseReference myRef = database.getReference();
    String userID;
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    public AddPlaylistDialogAdapter(ArrayList<Playlist> playlists, AddPlaylistDialogAdapter.OnPlaylistListener mOnPlaylistListener) {
        this.playlists = playlists;
        this.mOnPlaylistListener = mOnPlaylistListener;
    }

    public AddPlaylistDialogAdapter(List<Playlist> playLists) {

    }

    @NonNull
    @Override
    public AddPlaylistDialogAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.playlist_item2,parent,false);
        AddPlaylistDialogAdapter.ViewHolder holder = new AddPlaylistDialogAdapter.ViewHolder(view,mOnPlaylistListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull AddPlaylistDialogAdapter.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        userID = user.getUid();

        holder.txtName.setText(playlists.get(position).getNameOfPlaylist());

    }

    @Override
    public int getItemCount() {
        if(playlists != null) {
            return playlists.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView txtName;
        private RelativeLayout parent;
        AddPlaylistDialogAdapter.OnPlaylistListener onPlaylistListener;

        public ViewHolder(@NonNull View itemView, AddPlaylistDialogAdapter.OnPlaylistListener onPlaylistListener) {
            super(itemView);
            txtName = itemView.findViewById(R.id.plName);
            parent = itemView.findViewById(R.id.playlist_item);
            this.onPlaylistListener = onPlaylistListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onPlaylistListener.onPlaylistClick(getAdapterPosition());
        }
    }
    public interface OnPlaylistListener{
        void onPlaylistClick(int position);
    }

    public boolean removePlaylist(Playlist playlist) {
        return playlists.remove(playlist);
    }
}
