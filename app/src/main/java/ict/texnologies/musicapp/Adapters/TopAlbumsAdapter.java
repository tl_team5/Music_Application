package ict.texnologies.musicapp.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

import ict.texnologies.musicapp.Models.MostLovedAlbums.LovedAlbumsDTO;
import ict.texnologies.musicapp.R;

public class TopAlbumsAdapter extends RecyclerView.Adapter<TopAlbumsAdapter.ViewHolder> {
    private List<LovedAlbumsDTO> lovedAlbumsList;
    private OnItemListener mOnItemListener;
    FirebaseDatabase database = FirebaseDatabase.getInstance("https://music-app-2604f-default-rtdb.europe-west1.firebasedatabase.app");
    DatabaseReference myRef = database.getReference();
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    String userID = user.getUid();

    public TopAlbumsAdapter(List<LovedAlbumsDTO> lovedAlbumsList) {
    this.lovedAlbumsList = lovedAlbumsList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.top_albums_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
            holder.tvAlbumName.setText(lovedAlbumsList.get(position).getStrAlbum());
            holder.tvArtistName.setText(lovedAlbumsList.get(position).getStrArtistStripped());
    }

    @Override
    public int getItemCount() {
        return lovedAlbumsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ArtistSearchAdapter.OnItemListener onItemListener;
        TextView tvAlbumName;
        TextView tvArtistName;
        TextView tvFavButton;
        public ViewHolder(@NonNull View itemView){
            super(itemView);
            tvAlbumName = itemView.findViewById(R.id.textViewAlbumName);
            tvArtistName = itemView.findViewById(R.id.textViewAlbArtistName);
            tvFavButton = itemView.findViewById(R.id.favBtn);
            this.onItemListener = onItemListener;
            itemView.setOnClickListener(this);
            tvFavButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    myRef.child("users").child(userID).child("fav_albums").child(lovedAlbumsList.get(position).getIdAlbum()).setValue(lovedAlbumsList.get(position).getStrAlbum());
                }
            });
        }

        @Override
        public void onClick(View view) {
            onItemListener.onItemClick(getAdapterPosition());
        }
    }
    public interface OnItemListener{
        void onItemClick(int position);
    }
}
