package ict.texnologies.musicapp.Adapters;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import ict.texnologies.musicapp.Models.Playlists.Playlist;
import ict.texnologies.musicapp.R;

public class PlaylistsRecyclerViewAdapter extends RecyclerView.Adapter<PlaylistsRecyclerViewAdapter.ViewHolder> {

    private OnPlaylistListener mOnPlaylistListener;
    private ArrayList<Playlist> playlists = new ArrayList<>();
    FirebaseDatabase database = FirebaseDatabase.getInstance("https://music-app-2604f-default-rtdb.europe-west1.firebasedatabase.app");
    DatabaseReference myRef = database.getReference();
    String userID;
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    public PlaylistsRecyclerViewAdapter(ArrayList<Playlist> playlists,OnPlaylistListener mOnPlaylistListener) {
        this.playlists = playlists;
        this.mOnPlaylistListener = mOnPlaylistListener;
    }

    public PlaylistsRecyclerViewAdapter(List<Playlist> playLists) {

    }

    //public PlaylistsRecyclerViewAdapter(Context mContext) { this.mContext = mContext;
     //    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.playlist_item,parent,false);
        ViewHolder holder = new ViewHolder(view,mOnPlaylistListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        userID = user.getUid();

        holder.txtName.setText(playlists.get(position).getNameOfPlaylist());
        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                builder.setMessage("Are you sure you want to delete " + playlists
                        .get(position).getNameOfPlaylist() + " Playlist?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                            myRef.child("users").child(userID).child("playlists").child(playlists.get(position).getNameOfPlaylist()).removeValue();
                            Toast.makeText(v.getContext(), "Playlist Deleted!!", Toast.LENGTH_SHORT).show();
                            notifyDataSetChanged();  // Refreshing RecyclerView

                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.create().show();
            }
        });

    }

    @Override
    public int getItemCount() {
        if(playlists != null) {
            return playlists.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView txtName;
        private Button btnDelete;
        private ConstraintLayout parent;
        OnPlaylistListener onPlaylistListener;

        public ViewHolder(@NonNull View itemView,OnPlaylistListener onPlaylistListener) {
            super(itemView);
            txtName = itemView.findViewById(R.id.txtName);
            btnDelete = itemView.findViewById(R.id.btnDelete);
            parent = itemView.findViewById(R.id.parent);
            this.onPlaylistListener = onPlaylistListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onPlaylistListener.onPlaylistClick(getAdapterPosition());
        }
    }
    public interface OnPlaylistListener{
        void onPlaylistClick(int position);
    }

    public boolean removePlaylist(Playlist playlist) {
        return playlists.remove(playlist);
    }
}
