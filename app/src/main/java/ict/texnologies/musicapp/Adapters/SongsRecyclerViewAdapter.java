package ict.texnologies.musicapp.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import ict.texnologies.musicapp.Models.Playlists.Song;
import ict.texnologies.musicapp.R;

public class SongsRecyclerViewAdapter extends RecyclerView.Adapter<SongsRecyclerViewAdapter.ViewHolder> {


    ArrayList<Song> songs = new ArrayList<>();
    private OnSongListener mOnSongListener;

    public SongsRecyclerViewAdapter(ArrayList<Song> songs, OnSongListener mOnSongListener) {
        this.songs = songs;
        this.mOnSongListener = mOnSongListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.song_item,parent,false);
        ViewHolder holder = new ViewHolder(view,mOnSongListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtNameOfSong.setText(songs.get(position).getNameOfSong());
        holder.txtNameOfSinger.setText(songs.get(position).getNameOfSinger());

    }
    @Override
    public int getItemCount() {
        if(songs != null) {
            return songs.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView txtNameOfSong;
        private TextView txtNameOfSinger;
        private ConstraintLayout parent1;
        OnSongListener onSongListener;
        public ViewHolder(@NonNull View itemView,OnSongListener onSongListener) {
            super(itemView);
            txtNameOfSong = itemView.findViewById(R.id.txtNameOfSong);
            txtNameOfSinger = itemView.findViewById(R.id.txtNameOfSinger);
            parent1 = itemView.findViewById(R.id.parent1);
            this.onSongListener = onSongListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onSongListener.onSongClick(getAdapterPosition());
        }
    }
    public interface  OnSongListener{
        void onSongClick(int position);
    }
}
