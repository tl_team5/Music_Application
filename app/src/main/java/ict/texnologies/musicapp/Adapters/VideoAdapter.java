package ict.texnologies.musicapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import ict.texnologies.musicapp.Models.MusicVideos.MVideoDTO;
import ict.texnologies.musicapp.R;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.ViewHolder> {

    private List<MVideoDTO> videoList;
    private OnVideoListener mOnVideoListener;
    Context context;
    public VideoAdapter(List<MVideoDTO> videoList,OnVideoListener onVideoListener,Context context) {
        this.videoList = videoList;
        this.mOnVideoListener = onVideoListener;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.video_item,parent,false);

        return new ViewHolder(v,mOnVideoListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvVideoName.setText(videoList.get(position).getStrTrack());
        Glide.with(this.context)
                .load(videoList.get(position).getStrTrackThumb())
                .into(holder.ivVideo);
    }

    @Override
    public int getItemCount() {
        return videoList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView tvVideoName;
        ImageView ivVideo;
        OnVideoListener onVideoListener;
        public ViewHolder(@NonNull View itemView,OnVideoListener onVideoListener) {
            super(itemView);
            tvVideoName = itemView.findViewById(R.id.textViewVideoName);
            ivVideo = itemView.findViewById(R.id.imageViewVideo);
            this.onVideoListener =onVideoListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onVideoListener.onVideoClick(getAdapterPosition());
        }
    }
    public interface  OnVideoListener{
        void onVideoClick(int position);
    }
}
