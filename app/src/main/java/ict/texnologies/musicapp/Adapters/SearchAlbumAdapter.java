package ict.texnologies.musicapp.Adapters;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;



import java.util.List;

import ict.texnologies.musicapp.Models.AlbumSearch.SearchAlbumDTO;


import ict.texnologies.musicapp.R;

public class SearchAlbumAdapter extends RecyclerView.Adapter<SearchAlbumAdapter.ViewHolder>{

    private List<SearchAlbumDTO> albumList;

    public SearchAlbumAdapter(List<SearchAlbumDTO> albumList) {
        this.albumList = albumList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.album_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvAlbumName.setText(albumList.get(position).getName());
        holder.tvArtistName.setText(albumList.get(position).getArtist());

    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView tvAlbumName;
        TextView tvArtistName;
        public ViewHolder(@NonNull View itemView){
            super(itemView);
            tvAlbumName = itemView.findViewById(R.id.textViewName);
            tvArtistName = itemView.findViewById(R.id.textViewDate);
        }
    }

}