package ict.texnologies.musicapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ict.texnologies.musicapp.Models.ArtistDiscography.AlbumDTO;
import ict.texnologies.musicapp.R;

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.ViewHolder> {
    private List<AlbumDTO> albumsList;
    private OnAlbumListener mOnAlbumListener;
    Context context;

    public AlbumAdapter(List<AlbumDTO> albumsList) {
        this.albumsList = albumsList;
    }

    public AlbumAdapter(List<AlbumDTO> albumsList, OnAlbumListener onAlbumListener, Context context) {
        this.albumsList = albumsList;
        this.mOnAlbumListener = onAlbumListener;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.album_item,parent,false);

        return new ViewHolder(v,mOnAlbumListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvName.setText(albumsList.get(position).getStrAlbum());
        holder.tvDate.setText(""+albumsList.get(position).getIntYearReleased());
    }

    @Override
    public int getItemCount() {
        return albumsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView tvName;
        TextView tvDate;
        OnAlbumListener onAlbumListener;
        public ViewHolder(@NonNull View itemView,OnAlbumListener onAlbumListener) {
            super(itemView);
            tvName = itemView.findViewById(R.id.textViewName);
            tvDate = itemView.findViewById(R.id.textViewDate);
            this.onAlbumListener = onAlbumListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onAlbumListener.onAlbumClick(getAdapterPosition());
        }
    }
    public interface  OnAlbumListener{
        void onAlbumClick(int position);
    }
}
