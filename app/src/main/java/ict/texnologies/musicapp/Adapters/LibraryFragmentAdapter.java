package ict.texnologies.musicapp.Adapters;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import ict.texnologies.musicapp.Views.AddPlaylistsFragment;
import ict.texnologies.musicapp.Views.LikedFragment;

public class LibraryFragmentAdapter extends FragmentStateAdapter {


    public LibraryFragmentAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position)
        {
            case 1:
                return new LikedFragment();
            case 2:
//                return new TrackFragment();
        }
        return new AddPlaylistsFragment();
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}
