package ict.texnologies.musicapp.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ict.texnologies.musicapp.Models.ArtistInfo.InformationDTO;
import ict.texnologies.musicapp.R;

public class ArtistSearchAdapter extends RecyclerView.Adapter<ArtistSearchAdapter.ViewHolder>{

    private List<InformationDTO> infoList;
    private OnItemListener mOnItemListener;
    public ArtistSearchAdapter(List<InformationDTO> infoList, OnItemListener onItemListener) {
        this.infoList = infoList;
        this.mOnItemListener = onItemListener;
    }

    @NonNull
    @Override
    public ArtistSearchAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.artist_item,parent,false);
        return new ViewHolder(v,mOnItemListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ArtistSearchAdapter.ViewHolder holder, int position) {
        holder.tvName.setText(infoList.get(position).getStrArtist());
        holder.tvStyle.setText(infoList.get(position).getStrStyle());
    }

    @Override
    public int getItemCount() {
        return infoList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView tvName;
        TextView tvStyle;
        OnItemListener onItemListener;
        public ViewHolder(@NonNull View itemView,OnItemListener onItemListener) {
            super(itemView);
            tvName = itemView.findViewById(R.id.textViewArtistName);
            tvStyle = itemView.findViewById(R.id.textViewStyle);
            this.onItemListener = onItemListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onItemListener.onItemClick(getAdapterPosition());
        }
    }
    public interface OnItemListener{
        void onItemClick(int position);
    }
}
