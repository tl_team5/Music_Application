package ict.texnologies.musicapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import ict.texnologies.musicapp.Models.ArtistSearch.SearchTrackDTO;

import ict.texnologies.musicapp.Models.Tracks.TrackDTO;
import ict.texnologies.musicapp.R;

public class SearchTrackAdapter extends RecyclerView.Adapter<SearchTrackAdapter.ViewHolder> {

    private List<SearchTrackDTO> tracksList;
    private OnTrackListener mOnTrackListener;
    Context context;

    public SearchTrackAdapter(List<SearchTrackDTO> tracksList,OnTrackListener onTrackListener, Context context) {
        this.tracksList = tracksList;
        this.mOnTrackListener = onTrackListener;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.track_item,parent,false);
        return new ViewHolder(v,mOnTrackListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvName.setText(tracksList.get(position).getName());
        holder.tvPlaycount.setText(tracksList.get(position).getListeners());
        Glide.with(this.context)
                .load(tracksList.get(position).getImage().get(0).getText())
                .into(holder.ivTrack);
    }

    @Override
    public int getItemCount() {
        return tracksList.size();
    }

    public class ViewHolder  extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{
        TextView tvName;
        TextView tvPlaycount;
        ImageView ivTrack;
       OnTrackListener onTrackListener;
        public ViewHolder(@NonNull View itemView, SearchTrackAdapter.OnTrackListener onTrackListener) {
            super(itemView);
            tvName = itemView.findViewById(R.id.textViewTrackName);
            tvPlaycount = itemView.findViewById(R.id.textViewPlaycount);
            ivTrack = itemView.findViewById(R.id.imageViewTrack);
            this.onTrackListener = onTrackListener;
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onTrackListener.onTrackClick(getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View view) {
            onTrackListener.onTrackLongClick(getAdapterPosition());
            return true;
        }


    }
    public interface OnTrackListener{
        void onTrackClick(int position);
        void onTrackLongClick(int position);
    }
    // method for filtering our recyclerview items

}
