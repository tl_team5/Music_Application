package ict.texnologies.musicapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

import ict.texnologies.musicapp.Models.Tracks.TrackDTO;
import ict.texnologies.musicapp.R;

public class TrackAdapter extends RecyclerView.Adapter<TrackAdapter.ViewHolder> {

    private List<TrackDTO> tracksList;
    private OnTrackListener mOnTrackListener;
    private TopAlbumsAdapter.OnItemListener mOnItemListener;
    FirebaseDatabase database = FirebaseDatabase.getInstance("https://music-app-2604f-default-rtdb.europe-west1.firebasedatabase.app");
    DatabaseReference myRef = database.getReference();
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    String userID = user.getUid();

    Context context;
    public TrackAdapter(List<TrackDTO> tracksList,OnTrackListener onTrackListener,Context context) {
        this.tracksList = tracksList;
        this.mOnTrackListener = onTrackListener;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.track_item,parent,false);
        return new ViewHolder(v,mOnTrackListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvName.setText(tracksList.get(position).getName());
        holder.tvPlaycount.setText(tracksList.get(position).getPlaycount());
        Glide.with(this.context)
                .load(tracksList.get(position).getImage().get(0).getText())
                .into(holder.ivTrack);
    }

    @Override
    public int getItemCount() {
        return tracksList.size();
    }

    public class ViewHolder  extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener{
        TextView tvName;
        TextView tvPlaycount;
        TextView tvFavButton;
        ImageView ivTrack;
        OnTrackListener onTrackListener;
        public ViewHolder(@NonNull View itemView,OnTrackListener onTrackListener) {
            super(itemView);
            tvName = itemView.findViewById(R.id.textViewTrackName);
            tvPlaycount = itemView.findViewById(R.id.textViewPlaycount);
            ivTrack = itemView.findViewById(R.id.imageViewTrack);
            tvFavButton = itemView.findViewById(R.id.favBtn);

            itemView.setOnClickListener(this);
            tvFavButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    myRef.child("users").child(userID).child("fav_tracks").child(tracksList.get(position).getName()).setValue(tracksList.get(position).getName());
                }
            });

            this.onTrackListener = onTrackListener;
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onTrackListener.onTrackClick(getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View view) {
            onTrackListener.onTrackLongClick(getAdapterPosition());
            return true;
        }


    }
    public interface OnTrackListener{
        void onTrackClick(int position);
        void onTrackLongClick(int position);
    }

    // method for filtering our recyclerview items
    public void filterList(List<TrackDTO> filterllist) {
        // below line is to add our filtered
        // list in our course array list.
        tracksList = filterllist;
        // below line is to notify our adapter
        // as change in recycler view data.
        notifyDataSetChanged();
    }
}
