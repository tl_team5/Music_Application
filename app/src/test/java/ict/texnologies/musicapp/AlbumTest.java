package ict.texnologies.musicapp;

import org.junit.Test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import ict.texnologies.musicapp.Models.ArtistDiscography.AlbumDTO;
import ict.texnologies.musicapp.Models.ArtistDiscography.AlbumList;

public class AlbumTest {
    @Test
    public void checkIfArrayListAlbumIsNotEmpty(){
        ArrayList<AlbumDTO> albums = new ArrayList<>();
        AlbumDTO albumDTO = new AlbumDTO("randomAlbum","1928");
        albums.add(albumDTO);
        assertFalse(albums.isEmpty());
    }
    @Test
    public void checkIfNameOfAlbumIsEmpty(){
        AlbumDTO albumDTO = new AlbumDTO("","1928");
        String name = albumDTO.getStrAlbum();
        assertNotEquals("randomAlbum",name);
    }
    @Test
    public void checkIfYearOfAlbumIsEmpty(){
        AlbumDTO albumDTO = new AlbumDTO("randomAlbum","");
        String year = albumDTO.getIntYearReleased();
        assertNotEquals("1928",year);
    }
    @Test
    public void checkIfNameOfAlbumExists(){
        AlbumDTO albumDTO = new AlbumDTO("randomAlbum","1928");
        String name = albumDTO.getStrAlbum();
        assertEquals("randomAlbum",name);
    }
    @Test
    public void checkIfYearOfAlbumExists(){
        AlbumDTO albumDTO = new AlbumDTO("randomAlbum","1928");
        String year = albumDTO.getIntYearReleased();
        assertEquals("1928",year);
    }
    @Test
    public void testSetIntYearRelease() {
        AlbumDTO albumDTO = new AlbumDTO("randomAlbum", "1928");
        albumDTO.setIntYearReleased("1194");
        String year = albumDTO.getIntYearReleased();
        assertEquals("1194",year);
    }
    @Test
    public void testSetAlbumName(){
        AlbumDTO albumDTO = new AlbumDTO("randomAlbum", "1928");
        albumDTO.setStrAlbum("AnotherRandomAlbum");
        String name = albumDTO.getStrAlbum();
        assertEquals("AnotherRandomAlbum",name);
    }
    @Test
    public void testAlbumListConstructor(){
        ArrayList<AlbumDTO> albums = new ArrayList<>();
        AlbumDTO albumDTO = new AlbumDTO("randomAlbum","1928");
        albums.add(albumDTO);
        AlbumList albumList = new AlbumList(albums);
        assertFalse(albumList.getAlbum().isEmpty());
    }
    @Test
    public void testSetAlbum(){
        ArrayList<AlbumDTO> albums = new ArrayList<>();
        ArrayList<AlbumDTO> albums2 = new ArrayList<>();
        AlbumDTO albumDTO = new AlbumDTO("randomAlbum","1928");
        albums.add(albumDTO);
        albums2.add(albumDTO);
        AlbumList albumList = new AlbumList(albums);
        albumList.setAlbum(albums2);
        albumList.getAlbum();
        assertEquals(albums2,albumList.getAlbum());

    }

}

