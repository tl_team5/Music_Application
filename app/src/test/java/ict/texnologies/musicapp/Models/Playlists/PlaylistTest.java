package ict.texnologies.musicapp.Models.Playlists;

import static org.junit.Assert.*;

import org.junit.Test;

import java.util.ArrayList;

public class PlaylistTest {


    @Test
    public void testEmptyArrayListPlaylists() {
        ArrayList<Playlist> playlists = new ArrayList<>();
        assertTrue(playlists.isEmpty());
    }

    @Test
    public void checkIfArrayListPlaylistsIsNotEmpty(){
        ArrayList<Playlist> playlists = new ArrayList<>();
        ArrayList<Song> songs = new ArrayList<>();

        Playlist result = new Playlist("Pop",songs);

        playlists.add(result);

        assertFalse(playlists.isEmpty());
    }

    @Test
    public void checkIfNameOfPlaylistIsEmpty(){

        ArrayList<Song> songs = new ArrayList<>();
        Playlist playlist = new Playlist("",songs);
        String result = playlist.getNameOfPlaylist();

        assertNotEquals("Pop", result);
    }

    @Test
    public void checkIfNameOfPlaylistExists(){
        ArrayList<Song> songs = new ArrayList<>();
        Playlist playlist = new Playlist("Pop",songs);
        String result = playlist.getNameOfPlaylist();

        assertEquals("Pop", result);
    }

    @Test
    public void checkIfArrayListSongsIsEmpty(){
        ArrayList<Song> songs = null;
        Playlist playlist = new Playlist("Pop",songs);
        ArrayList<Song> result = playlist.getSongs();

        assertNotEquals("songs", result);
    }

    @Test
    public void checkIfArrayListSongsExists(){
        ArrayList<Song> songs = new ArrayList<>();
        Playlist playlist = new Playlist("Pop",songs);
        ArrayList<Song> result = playlist.getSongs();

        assertEquals(songs, result);
    }
}