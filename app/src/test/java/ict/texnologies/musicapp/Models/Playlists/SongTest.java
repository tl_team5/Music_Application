package ict.texnologies.musicapp.Models.Playlists;

import static org.junit.Assert.*;

import org.junit.Test;

import java.util.ArrayList;

public class SongTest {

    @Test
    public void testEmptyArrayListSong() {
        ArrayList<Song> songs = new ArrayList<>();
        assertTrue(songs.isEmpty());
    }

    @Test
    public void checkIfArrayListSongIsNotEmpty(){
        ArrayList<Song> songs = new ArrayList<>();
        Song result = new Song("My Universe","Coldplay",
                "https://www.last.fm/music/Coldplay/_/My+Universe");
        songs.add(result);

        assertFalse(songs.isEmpty());
    }

    @Test
    public void checkIfNameOfSongIsEmpty(){
        Song song = new Song("","Coldplay",
                "https://www.last.fm/music/Coldplay/_/My+Universe");
        String result = song.getNameOfSong();

        assertNotEquals("My Universe", result);
    }

    @Test
    public void checkIfNameOfSongExists(){
        Song song = new Song("My Universe","Coldplay",
                "https://www.last.fm/music/Coldplay/_/My+Universe");
        String result = song.getNameOfSong();

        assertEquals("My Universe", result);
    }

    @Test
    public void checkIfNameOfSingerIsEmpty(){
        Song song = new Song("My Universe","",
                "https://www.last.fm/music/Coldplay/_/My+Universe");
        String result = song.getNameOfSinger();

        assertNotEquals("Coldplay", result);
    }

    @Test
    public void checkIfNameOfSingerExists(){
        Song song = new Song("My Universe","Coldplay",
                "https://www.last.fm/music/Coldplay/_/My+Universe");
        String result = song.getNameOfSinger();

        assertEquals("Coldplay", result);
    }

    @Test
    public void checkIfSongUrlIsEmpty(){
        Song song = new Song("My Universe","Coldplay",
                "");
        String result = song.getSongUrl();

        assertNotEquals("https://www.last.fm/music/Coldplay/_/My+Universe", result);
    }

    @Test
    public void checkIfSongUrlExists(){
        Song song = new Song("My Universe","Coldplay",
                "https://www.last.fm/music/Coldplay/_/My+Universe");
        String result = song.getSongUrl();

        assertEquals("https://www.last.fm/music/Coldplay/_/My+Universe", result);
    }


}