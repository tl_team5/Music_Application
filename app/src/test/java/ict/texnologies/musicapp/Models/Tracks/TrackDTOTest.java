package ict.texnologies.musicapp.Models.Tracks;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TrackDTOTest {

    ArrayList<TrackDTO> tracks = new ArrayList<>();
    Streamable streamable;
    Artist artist;
    List<Image> images;
    Image image1;
    Image image2;
    Image image3;
    Image image4;
    TrackDTO trackDTO;

    @Before
    public void setUp(){
        streamable = new Streamable("0","0");
        artist = new Artist("Adele","cc2c9c3c-b7bc-4b8b-84d8-4fbd8779e493",
                "https://www.last.fm/music/Adele");
        images = new ArrayList<>();
        image1 = new Image("https://lastfm.freetls.fastly.net/i/u" +
                "/34s/2a96cbd8b46e442fc41c2b86b821562f.png","small");
        image2 = new Image("https://lastfm.freetls.fastly.net/i/u" +
                "/64s/2a96cbd8b46e442fc41c2b86b821562f.png","medium");
        image3 = new Image("https://lastfm.freetls.fastly.net/i/u" +
                "/174s/2a96cbd8b46e442fc41c2b86b821562f.png","large");
        image4 = new Image("https://lastfm.freetls.fastly.net/i/u" +
                "/300x300/2a96cbd8b46e442fc41c2b86b821562f.png","extralarge");
        images.add(image1);
        images.add(image2);
        images.add(image3);
        images.add(image4);

        trackDTO = new TrackDTO("Easy On ME", "0",
                "3758249", "392065", "",
                "https://www.last.fm/music/Adele/_/Easy+On+ME",
                streamable, artist, images);
    }

    @Test
    public void checkEmptyArrayListTrackDTO(){

        assertTrue(tracks.isEmpty());
    }


    @Test
    public void checkIfArrayListTracksDTOIsNotEmpty(){

        tracks.add(trackDTO);
        assertFalse(tracks.isEmpty());
    }

    @Test
    public void checkIfNameIsEmpty(){

        trackDTO.setName("");
        String result = trackDTO.getName();

        assertNotEquals("Easy On ME",result);
    }

    @Test
    public void checkIfNameIsExists(){

        String result = trackDTO.getName();

        assertEquals("Easy On ME",result);
    }

    @Test
    public void checkIfDurationIsEmpty(){

        trackDTO.setDuration("");
        String result = trackDTO.getDuration();

        assertNotEquals("0",result);
    }

    @Test
    public void checkIfDurationIsExists(){

        String result = trackDTO.getDuration();

        assertEquals("0",result);
    }

    @Test
    public void checkIfPlaycountIsEmpty(){

        trackDTO.setPlaycount("");
        String result = trackDTO.getPlaycount();

        assertNotEquals("3758249",result);
    }

    @Test
    public void checkIfPlaycountIsExists(){

        String result = trackDTO.getPlaycount();

        assertEquals("3758249",result);
    }

    @Test
    public void checkIfListenersIsEmpty(){

        trackDTO.setListeners("");
        String result = trackDTO.getListeners();

        assertNotEquals("392065",result);

    }

    @Test
    public void checkIfListenersExists(){

        String result = trackDTO.getListeners();

        assertEquals("392065",result);

    }

    @Test
    public void checkIfMbidIsEmpty(){

        String result = trackDTO.getMbid();

        assertEquals("",result);

    }

    @Test
    public void checkIfMbidExists(){

        String result = trackDTO.getMbid();

        assertNotEquals("something",result);

    }

    @Test
    public void checkIfUrlIsEmpty(){

        trackDTO.setUrl("");
        String result = trackDTO.getUrl();

        assertNotEquals("https://www.last.fm/music/Adele/_/Easy+On+ME",result);

    }

    @Test
    public void checkIfUrlExists(){

        String result = trackDTO.getUrl();

        assertEquals("https://www.last.fm/music/Adele/_/Easy+On+ME",result);

    }

    @Test
    public void checkIfStreamableIsEmpty(){

        Streamable expectedStreamable = new Streamable("","");
        Streamable result = trackDTO.getStreamable();

        assertNotEquals(expectedStreamable.getText(),result.getText());
        assertNotEquals(expectedStreamable.getFulltrack(), result.getFulltrack());
    }

    @Test
    public void checkIfStreamableExists(){

        Streamable expectedStreamable = new Streamable("0","0");
        Streamable result = trackDTO.getStreamable();

        assertEquals(expectedStreamable.getText(), result.getText());
        assertEquals(expectedStreamable.getFulltrack(), result.getFulltrack());
    }

    @Test
    public void checkIfArtistIsEmpty(){

        Artist expectedArtist = new Artist("","","");
        Artist result = trackDTO.getArtist();

        assertNotEquals(expectedArtist.getName(),result.getName());
        assertNotEquals(expectedArtist.getMbid(), result.getMbid());
        assertNotEquals(expectedArtist.getUrl(), result.getUrl());
    }

    @Test
    public void checkIfArtistExists(){

        Artist expectedArtist = new Artist("Adele",
                "cc2c9c3c-b7bc-4b8b-84d8-4fbd8779e493",
                "https://www.last.fm/music/Adele");
        Artist result = trackDTO.getArtist();

        assertEquals(expectedArtist.getName(), result.getName());
        assertEquals(expectedArtist.getMbid(), result.getMbid());
        assertEquals(expectedArtist.getUrl(), result.getUrl());
    }

    @Test
    public void checkIfImageIsEmpty(){

        List<Image> expectedImages = new ArrayList<>();
        List<Image> result = trackDTO.getImage();

        assertNotEquals(expectedImages, result);
    }

    @Test
    public void checkIfImageExists(){

        List<Image> expectedImages = new ArrayList<>();
        expectedImages.add(image1);
        expectedImages.add(image2);
        expectedImages.add(image3);
        expectedImages.add(image4);
        List<Image> result = trackDTO.getImage();

        assertTrue(expectedImages.toString().contentEquals(result.toString()));
    }


}